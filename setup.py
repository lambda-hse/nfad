"""
(1 + eps)-class classification with normalizing flows experiments.
"""

from setuptools import setup, find_packages
import os

here = os.path.dirname(__file__)

try:
  with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description=f.read()
except:
  long_description=''

version = '0.0.1'

setup(
  name = 'openf',
  version=version,
  description="""OPE with Normalizing Flows""",

  long_description=long_description,
  long_description_content_type="text/markdown",

  url='https://gitlab.com/lambda-hse/1-plus-eps-nf-experiments/',

  author='HSE LAMBDA',
  author_email='',

  maintainer='HSE LAMBDA',
  maintainer_email='',

  license='MIT',

  classifiers=[
    'Development Status :: 4 - Beta',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3',
  ],

  packages=find_packages(exclude=['contrib', 'examples', 'docs', 'tests', 'scripts']),

  extras_require={
    'test': [
      'pytest >= 4.0.0',
      'scipy >= 1.4.0'
    ],
  },

  install_requires=[
    'numpy >= 1.18.5',
    'scikit-learn >= 0.23.2',
    'torch >= 1.7.0',
    'torchvision >= 0.8.1',
    'keras >= 2.4.3',
    'crayflow >= 0.3.2'
  ],

  python_requires='>=3.7',
)


