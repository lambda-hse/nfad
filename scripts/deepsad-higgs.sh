#!/bin/bash

mkdir -p logs/

python scripts/warmup.py higgs || exit $?

for partition in 0 100 1000 10000 1000000; do
  FILENAME="DeepSAD-HIGGS-$partition"
  sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
    scripts/main.sh dsad higgs --anomalies "$partition" --device cuda --epoches 10 --batch_size 100 "$@" &
done

wait
