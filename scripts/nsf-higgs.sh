#!/bin/bash

mkdir -p logs/

python scripts/warmup.py higgs || exit $?

for partition in 0 100 1000 10000 1000000; do
  FILENAME="NSF-HIGGS-$normal-$partition"
  sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
    scripts/main.sh nsf higgs --anomalies "$partition" --device cuda "$@" &
done

wait
