#!/bin/bash

for trial in {0..9}; do
  python scripts/main.py "$@" --seed "${trial}12345"
done
