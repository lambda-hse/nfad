# from openf.models.mnist import MNIST_LeNet_Encoder, MNIST_LeNet_Decoder
# from openf.models import CIFAR10_LeNet_Decoder, CIFAR10_LeNet_Encoder
# from openf.models.omniglot import Omniglot_LeNet_Encoder, Omniglot_LeNet_Decoder
# from openf.models import Dense_Encoder, Dense_Decoder
# from openf.algorithms.devnet import DevNet

# From https://arxiv.org/pdf/1906.06096.pdf
# n_anomalies = {
#     'higgs': [0, 100, 1000, 10000, 1000000],
#     'susy': [0, 100, 1000, 10000, 1000000],
# }
#
# n_anomaly_classes = {
#     'kdd': [0, 1, 2, 4, 8],
#     'mnist': [0, 1, 2, 4],
#     'cifar': [0, 1, 2, 4],
#     'omniglot': [0, 1, 2, 4],
# }

import numpy as np
import matplotlib.pyplot as plt

from tqdm import tqdm, trange
import gc
import os
import sys; sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../')
sys.path.append('../')

def eval_dsad(dataset, n_anomalies, n_epoches, batch_size, device, seed, progress=None):
    import torch
    torch.manual_seed(seed + 1)

    device = torch.device(device)

    pool = dataset.network_pool()
    encoder, decoder = pool['encoder'](), pool['decoder']()
    encoder = encoder.to(device)
    decoder = decoder.to(device)

    data_pos, data_neg = dataset.partition(n_anomalies, seed=seed)
    print('Deep-SAD, %s, seed=%d' % (dataset, seed))
    print('Positive samples: %d' % (len(data_pos), ))
    print('Negative samples: %d' % (len(data_neg) if data_neg is not None else 0,))
    X_pos = torch.tensor(data_pos, dtype=torch.float32, device=device, requires_grad=False)

    if data_neg is None:
        X_neg = None
    else:
        X_neg = torch.tensor(data_neg, dtype=torch.float32, device=device, requires_grad=False)

    from openf.algorithms import deep_sad
    pretrain_losses = deep_sad.pretrain_dsad(
        encoder, decoder,
        X_pos=X_pos, X_neg=X_neg,
        batch_size=batch_size,
        n_epoches=2 * n_epoches,
        progress=progress,
    )
    losses = deep_sad.train_dsad(
        encoder,
        X_pos=X_pos, X_neg=X_neg,
        batch_size=batch_size,
        n_epoches=n_epoches,
        progress=progress,
    )

    data_test, labels_test = dataset.data_test, dataset.labels_test
    X_test = torch.tensor(data_test, dtype=torch.float32, device=device, requires_grad=False)

    predictions = deep_sad.predict_dsad(encoder, X_test, batch_size=batch_size)

    from sklearn.metrics import roc_auc_score
    ### the lower the predictions (which is a distance), the higher the score of the normal class
    score = 1 - roc_auc_score(labels_test, predictions)

    return pretrain_losses, losses, score, encoder.state_dict()

def eval_resflow(dataset, n_anomalies, device, seed, **kwargs):
    import torch
    from openf.algorithms import resflow_utils as resflow
    torch.manual_seed(seed + 1)

    device = torch.device(device)
    n_colors = {'Omniglot':1, 'MNIST': 1, 'CIFAR': 3}[dataset.__class__.__name__]
    kwargs['im_dim'] = n_colors
    kwargs['data'] = dataset.__class__.__name__.lower()

    pool = dataset.network_pool()
    nf, input_size, squeeze_layer = pool['nf'](**kwargs)
    nf = nf.to(device)
    args = resflow.get_args(**kwargs)
    opt_pdf, opt_clf = resflow.build_optimizers(nf, args)
    from openf.algorithms.lib.utils import ExponentialMovingAverage
    ema = ExponentialMovingAverage(nf)

    data_pos, data_neg = dataset.partition(n_anomalies, seed=seed)
    from openf.datasets.utils import combine
    data_train, labels_train = combine(data_pos, data_neg)


    print('Deep-SAD, %s, seed=%d' % (dataset, seed))
    print('Positive samples: %d' % (len(data_pos), ))
    print('Negative samples: %d' % (len(data_neg) if data_neg is not None else 0,))

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    X_train = torch.tensor(data_train, dtype=torch.float32, requires_grad=False)
    y_train = torch.tensor(labels_train, dtype=torch.float32, requires_grad=False)
    data_test, labels_test = dataset.data_test, dataset.labels_test
    X_test = torch.tensor(data_test, dtype=torch.float32, requires_grad=False)

    import torch.nn.functional as F
    image_size = 32
    X_train, X_test = [F.interpolate(x,image_size) for x in [X_train, X_test]]

    dataloader_train = torch.utils.data.DataLoader(
        torch.utils.data.TensorDataset(X_train, y_train),
        shuffle=True, batch_size=args.batchsize,
    )
    dataloader_test = torch.utils.data.DataLoader(torch.utils.data.TensorDataset(X_test), shuffle=False, batch_size=args.batchsize)


    meter = resflow.build_meter()
    losses_nf, losses_ce = [], []
    for epoch in trange(args.nepochs_nf):
        resflow.train(epoch, nf, squeeze_layer, input_size, ema, opt_pdf, opt_clf, dataloader_train, args, device, meter, 'density')
        losses_nf.append(meter.bpd_meter.avg)

    for epoch in trange(args.nepochs_clf):
        resflow.train(epoch, nf, squeeze_layer, input_size, ema, opt_pdf, opt_clf, dataloader_train, args, device, meter, 'classification')
        losses_ce.append(meter.ce_meter.avg)


    predictions = resflow.predict(nf, squeeze_layer, input_size, dataloader_test, device, args)

    from sklearn.metrics import roc_auc_score
    ### the lower the predictions (which is a distance), the higher the score of the normal class
    try:
        score = 1 - roc_auc_score(labels_test, predictions[:,1])
    except ValueError:
        score = 1

    return losses_nf, losses_ce, score, nf.state_dict()

def eval_nf(dataset, n_anomalies, device, seed, **kwargs):
    import torch
    torch.manual_seed(seed + 1)

    from openf.algorithms import nsf
    from openf.datasets.utils import combine


    device = torch.device(device)

    n_features = dataset.shape()[1]
    pool = dataset.network_pool()
    nf, clf = pool['nf'](n_features, **kwargs), pool['clf']()
    nf, clf = [model.to(device) for model in [nf, clf]]

    data_pos, data_neg = dataset.partition(n_anomalies, seed=seed)
    data_train, labels_train = combine(data_pos, data_neg)

    X_train = torch.tensor(data_train, dtype=torch.float32, requires_grad=False)
    y_train = torch.tensor(labels_train, dtype=torch.float32, requires_grad=False)
    data_test, labels_test = dataset.data_test, dataset.labels_test
    X_test = torch.tensor(data_test, dtype=torch.float32, requires_grad=False)

    dataloader_train = torch.utils.data.DataLoader(
        torch.utils.data.TensorDataset(X_train, y_train),
        shuffle=True, batch_size=100,
    )
    dataloader_test = torch.utils.data.DataLoader(torch.utils.data.TensorDataset(X_test), shuffle=False, batch_size=32)
    data_pos, data_neg = dataset.partition(n_anomalies, seed=seed)
    data_train, labels_train = combine(data_pos, data_neg)

    X_train = torch.tensor(data_train, dtype=torch.float32, requires_grad=False)
    y_train = torch.tensor(labels_train, dtype=torch.float32, requires_grad=False)
    data_test, labels_test = dataset.data_test, dataset.labels_test
    X_test = torch.tensor(data_test, dtype=torch.float32, requires_grad=False)

    losses_nf = nsf.train_nf(nf, data_pos)

    sample_anomalies_fn = lambda anomalies_cnt: nsf.sample(nf, (anomalies_cnt, n_features), device)

    losses_clf = clf.fit(dataloader_train, sample_anomalies_fn)

    predictions = clf.predict(dataloader_test)

    from sklearn.metrics import roc_auc_score
    ### the lower the predictions (which is a distance), the higher the score of the normal class
    score = 1 - roc_auc_score(labels_test, predictions)

    return losses_nf, losses_clf, score

def eval_feawad(dataset, n_anomalies, device, seed, **kwargs):
    from openf.algorithms import feawad
    from openf.datasets.utils import combine
    import numpy as np
    np.random.seed(42)
    import tensorflow as tf
    #tf.set_random_seed(42)
    #sess = tf.Session()

    from tensorflow.keras import backend as K

    from tensorflow.keras.models import Model
    from tensorflow.keras.layers import Input, Dense, Subtract,concatenate,Lambda,Reshape
    from tensorflow.keras.optimizers import Adam
    from tensorflow.keras.callbacks import ModelCheckpoint
    from tensorflow.keras.losses import mean_squared_error


    import argparse
    import numpy as np
    import sys
    from scipy.sparse import vstack, csc_matrix
    from openf.algorithms.feawad import dataLoading, aucPerformance, writeResults
    from sklearn.model_selection import train_test_split


    data_pos, data_neg = dataset.partition(n_anomalies, seed=seed)
    data_train, labels_train = combine(data_pos, data_neg)

    train_x, train_label = data_train, 1 - labels_train
    test_x, test_label = dataset.data_test, 1 - dataset.labels_test

    args = feawad.get_args()
    network_depth = int(args.network_depth)
    random_seed = args.ramdn_seed

    outlier_indices = np.where(train_label == 1)[0]
    outliers = train_x[outlier_indices]
    inlier_indices = np.where(train_label == 0)[0]
    n_outliers = len(outlier_indices)
    # print("Original training size: %d, Number of outliers in Train data:: %d" % (train_x.shape[0], n_outliers))
            
    n_noise  = len(np.where(train_label == 0)[0]) * args.cont_rate / (1. - args.cont_rate)
    n_noise = int(n_noise)    
    
    # Contamination ?
    rng = np.random.RandomState(random_seed)  
    if n_outliers > args.known_outliers:
        mn = n_outliers - args.known_outliers
        remove_idx = rng.choice(outlier_indices, mn, replace=False)
        train_x = np.delete(train_x, remove_idx, axis=0)
        train_label = np.delete(train_label, remove_idx, axis=0)
        #ae_label = train_x
    noises = feawad.inject_noise(outliers, n_noise, random_seed)
    train_x = np.append(train_x, noises, axis = 0)
    train_label = np.append(train_label, np.zeros((noises.shape[0], 1)))
                
    outlier_indices = np.where(train_label == 1)[0]
    inlier_indices = np.where(train_label == 0)[0]
    train_x_inlier = np.delete(train_x, outlier_indices, axis=0)
    print("Processed Train data number:",train_label.shape[0], "outliers number in Train data:",outlier_indices.shape[0],'\n',\
        "normal number in Train data:", inlier_indices.shape[0],"noise number:", n_noise)
    input_shape = train_x.shape[1:]
    n_samples_trn = train_x.shape[0]
    n_outliers = len(outlier_indices)

    n_samples_test = test_x.shape[0]
    test_outlier_indices = np.where(test_label == 1)[0]
    test_inlier_indices = np.where(test_label == 0)[0]
    print("Test data number:",test_label.shape[0],'\n',\
        "outliers number in Test data:",test_outlier_indices.shape[0],"normal number in Test data:",test_inlier_indices.shape[0])

    epochs = args.epochs
    batch_size = args.batch_size    
    nb_batch = args.nb_batch  

    AEmodel = feawad.deviation_network(input_shape,2,None,0)  #auto encoder model 预训练
    print('pre-training start....')
    print(AEmodel.summary())
    AEmodel_name = f"ckpt/{kwargs['job_name']}_auto_encoder_normalization.h5"
    ae_checkpointer = ModelCheckpoint(AEmodel_name, monitor='loss', verbose=0,
                                    save_best_only = True, save_weights_only = True)            
    AEmodel.fit_generator(feawad.auto_encoder_batch_generator_sup(train_x, inlier_indices, batch_size, nb_batch, rng),
                                        steps_per_epoch = nb_batch,
                                        epochs = 100,
                                        callbacks=[ae_checkpointer])

    print('load autoencoder model....')
    dev_model = feawad.deviation_network(input_shape, 4, AEmodel_name, 0)
    print('end-to-end training start....')
    dev_model_name = f"ckpt/{kwargs['job_name']}_devnet.h5"
    checkpointer = ModelCheckpoint(dev_model_name, monitor='loss', verbose=0,
                                    save_best_only = True, save_weights_only = True) 
    dev_model.fit_generator(feawad.batch_generator_sup(train_x, outlier_indices, inlier_indices, batch_size, nb_batch, rng),
                                    steps_per_epoch = nb_batch,
                                    epochs = epochs,
                                    callbacks=[checkpointer])
    print('load model and print results....')
    scores = feawad.load_model_weight_predict(dev_model_name, input_shape, 4, test_x)

    rauc, _ = feawad.aucPerformance(scores, test_label)

    ### the lower the predictions (which is a distance), the higher the score of the normal class
    score = 1 - rauc

    return score

def eval_pro(dataset, n_anomalies, n_epoches, device, seed):
    import torch
    from openf.algorithms.pro import AnomalyDetector
    
    torch.manual_seed(seed + 1)

    device = torch.device(device)

    pool = dataset.network_pool()
    encoder, anomaly_detector = pool['feature_encoder'](), pool['anomaly_detector']()
    encoder, anomaly_detector = [x.to(device) for x in [encoder, anomaly_detector]]

    
    data_pos, data_neg = dataset.partition(n_anomalies, seed=seed)
    model = AnomalyDetector(data_pos, data_neg, encoder, anomaly_detector)
    model.fit(epoches=n_epoches)
    
    # train_x, train_label = data_train, labels_train # 0 - anomaly, 1 - normal class
    test_x, test_label = dataset.data_test, 1-dataset.labels_test
    preds = model.predict(test_x)

    from sklearn.metrics import roc_auc_score
    ### the lower the predictions (which is a distance), the higher the score of the normal class
    score = 1 - roc_auc_score(test_label, preds)

    return score



def plot_losses(losses, filename):
    plt.figure(figsize=(9, 6))
    mean = np.mean(losses, axis=1)
    lower, upper = np.quantile(losses, q=0.2, axis=1), np.quantile(losses, q=0.8, axis=1)
    plt.plot(mean, color=plt.cm.tab10(0))
    plt.fill_between(np.arange(len(mean)), lower, upper, alpha=0.2)
    plt.savefig(filename)
    plt.close()

def main():
    import os
    import argparse
    from openf.datasets import available_datasets

    parser = argparse.ArgumentParser(description='Script to run experiments')
    parser.add_argument('model', type=str, choices=['dsad', 'devnet', 'resflow', 'nsf', 'iaf', 'feawad', 'pro'])
    parser.add_argument('dataset', type=str, choices=list(available_datasets.keys()),
                        help='Dataset to use')
    parser.add_argument('--anomalies', type=int, help='Number of anomalies or anomaly classes')
    parser.add_argument('--seed', type=int)
    parser.add_argument('--batch_size', type=int, default=32)
    parser.add_argument('--epoches', type=int, default=50)
    parser.add_argument('--normal', '-n', type=int, help='normal class', default=None)
    parser.add_argument('--device', type=str, help='device to perform computations on', default='cpu')
    parser.add_argument('--progress', action='store_true', default=False)
    parser.add_argument('--override', action='store_true', default=False)
    parser.add_argument('--data_root', type=str, default=os.environ.get('DATA_ROOT', '.'))

    args = parser.parse_args()

    import os
    os.makedirs('results/', exist_ok=True)

    print(args)

    if args.normal is not None:
        experiment_id = '%s-%d-%d-%s-%d' % (args.dataset, args.normal, args.anomalies, args.model, args.seed)
    else:
        experiment_id = '%s-%d-%s-%d' % (args.dataset, args.anomalies, args.model, args.seed)

    results_path = os.path.join('results', '%s.json' % (experiment_id, ))

    if os.path.exists(results_path) and not args.override:
        import warnings
        warnings.warn('File %s already exists!' % (results_path, ))
        return

    progress = tqdm if args.progress else None

    if args.normal is None:
        dataset = available_datasets[args.dataset](data_root=args.data_root)
    else:
        dataset = available_datasets[args.dataset](pos_class=args.normal, data_root=args.data_root)

    if args.model == 'dsad':
        prelosses, losses, score, parameters =  eval_dsad(
            dataset, args.anomalies,
            args.epoches, args.batch_size,
            device=args.device, seed=args.seed,
            progress=progress
        )
        plot_losses(
            prelosses,
            os.path.join('results/', '%s-pretrain.png' % (experiment_id, ))
        )
        plot_losses(
            losses,
            os.path.join('results/', '%s-train.png' % (experiment_id, ))
        )
        results = dict(
            dataset=args.dataset,
            normal=args.normal,
            n_anomalies=args.anomalies,
            model=args.model,
            seed=args.seed,
            score=score
        )

        with open(results_path, 'w') as f:
            import json
            json.dump(results, f, indent=2)

        print(results)

        with open(os.path.join('results/%s-parameters.pickled' % (experiment_id, )), 'wb') as f:
            import pickle
            pickle.dump(parameters, f)
    elif args.model == 'resflow':

        losses_nf, losses_ce, score, parameters = eval_resflow(
            dataset, args.anomalies,
            device=args.device, seed=args.seed
        )
#        plot_losses(
#            losses_nf,
#            os.path.join('results/', '%s-nf.png' % (experiment_id,))
#        )
#        plot_losses(
#            losses_ce,
#            os.path.join('results/', '%s-clf.png' % (experiment_id,))
#        )
        results = dict(
            dataset=args.dataset,
            normal=args.normal,
            n_anomalies=args.anomalies,
            model=args.model,
            seed=args.seed,
            score=score
        )

        with open(results_path, 'w') as f:
            import json
            json.dump(results, f, indent=2)

        print(results)

        with open(os.path.join('results/%s-parameters.pickled' % (experiment_id,)), 'wb') as f:
            import pickle
            pickle.dump(parameters, f)
    elif args.model in {'nsf', 'iaf'}:
        base_transform_type = {'nsf':'rq-autoregressive', 'iaf':'affine-autoregressive'}[args.model]
        losses_nf, losses_ce, score = eval_nf(
            dataset, args.anomalies,
            device=args.device, seed=args.seed, base_transform_type=base_transform_type
        )
#        plot_losses(
#            losses_nf,
#            os.path.join('results/', '%s-nf.png' % (experiment_id,))
#        )
#        plot_losses(
#            losses_ce,
#            os.path.join('results/', '%s-clf.png' % (experiment_id,))
#        )
        results = dict(
            dataset=args.dataset,
            normal=args.normal,
            n_anomalies=args.anomalies,
            model=args.model,
            seed=args.seed,
            score=score
        )

        with open(results_path, 'w') as f:
            import json
            json.dump(results, f, indent=2)

        print(results)

        # with open(os.path.join('results/%s-parameters.pickled' % (experiment_id,)), 'wb') as f:
        #     import pickle
        #     pickle.dump(parameters, f)
    elif args.model == 'feawad':
        score = eval_feawad(
            dataset, args.anomalies,
            device=args.device, seed=args.seed, 
            job_name=f'{args.dataset}_{args.anomalies}'
        )

        results = dict(
            dataset=args.dataset,
            normal=args.normal,
            n_anomalies=args.anomalies,
            model=args.model,
            seed=args.seed,
            score=score
        )

        with open(results_path, 'w') as f:
            import json
            json.dump(results, f, indent=2)

        print(results)
    
    elif args.model == 'pro':
        score = eval_pro(
            dataset, args.anomalies, args.epoches,
            device=args.device, seed=args.seed
        )

        results = dict(
            dataset=args.dataset,
            normal=args.normal,
            n_anomalies=args.anomalies,
            model=args.model,
            seed=args.seed,
            score=score
        )

        with open(results_path, 'w') as f:
            import json
            json.dump(results, f, indent=2)

        print(results)

    elif args.model == 'devnet':
        raise NotImplementedError()
    else:
        raise Exception('Not a valid method selection [%s]!' % (args.model, ))

if __name__ == '__main__':
    main()
