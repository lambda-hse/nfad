import os
import sys; sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../')

def main():
    import argparse
    from openf.datasets import available_datasets

    parser = argparse.ArgumentParser(description='Script to run experiments')
    parser.add_argument('dataset', type=str, choices=list(available_datasets.keys()),
                        help='Dataset to use')
    parser.add_argument('--data_root', type=str, default=os.environ.get('DATA_ROOT', '.'))
    args = parser.parse_args()

    print('Preparing %s...' % (args.dataset, ))

    try:
        available_datasets[args.dataset](data_root=args.data_root)
    except:
        available_datasets[args.dataset](0, data_root=args.data_root)

if __name__ == '__main__':
    main()
