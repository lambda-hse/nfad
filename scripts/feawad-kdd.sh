#!/bin/bash

mkdir -p logs/

python scripts/warmup.py kdd || exit $?

for partition in 0 1 2 4 8; do
  FILENAME="FEAWAD-KDD-$partition"
  sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
    scripts/main.sh feawad kdd --anomalies "$partition" --device cuda"$@" &
done

wait

