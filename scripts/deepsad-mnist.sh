#!/bin/bash

mkdir -p logs/

python scripts/warmup.py mnist || exit $?

for normal in {0..9}; do
  for partition in 0 1 2 4; do
    FILENAME="DeepSAD-MNIST-$normal-$partition"
    sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
      scripts/main.sh dsad mnist --normal "$normal" --anomalies "$partition" --device cuda --epoches 50 "$@" &
  done
done

wait
