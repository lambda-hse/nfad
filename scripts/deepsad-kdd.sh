#!/bin/bash

mkdir -p logs/

python scripts/warmup.py kdd || exit $?

for partition in 0 1 2 4 8; do
  FILENAME="DeepSAD-KDD-$partition"
  sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
    scripts/main.sh dsad kdd --anomalies "$partition" --device cuda --epoches 50 --batch_size 100 "$@" &
done

wait
