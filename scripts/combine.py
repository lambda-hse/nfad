import os
import json

import numpy as np

def main():
  results = list()

  for item in os.listdir('results/'):
    if not item.endswith('.json'):
      continue

    try:
      with open(os.path.join('results/', item), 'r') as f:
        results.append(json.load(f))
    except:
      import warnings
      warnings.warn('Failed to process %s' % (item, ))

  combined = dict()

  for result in results:
    key = (result['model'], result['dataset'], result['n_anomalies'])
    if key not in combined:
      combined[key] = list()

    combined[key].append(max(result['score'], 1-result['score']))

  combined = sorted(combined.items(), key=lambda k: k[0])

  for k, v in combined:
    print(
      ' '.join([ str(x).ljust(10) for x in k ]),
      '%.3lf +- %s' % (np.mean(v), ('%.3lf' % np.std(v, ddof=1)) if len(v) > 1 else '  nan'),
      '(Q1 = %.3lf, Q3 = %.3lf, N = %d)' % (np.quantile(v, q=0.25), np.quantile(v, q=0.75), len(v))
    )

if __name__ == '__main__':
    main()
