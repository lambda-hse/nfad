#!/bin/bash

mkdir -p logs/

python scripts/warmup.py omniglot || exit $?

for normal in 0 1 2; do
  for partition in 0 1 2 4; do
    FILENAME="ResFlow-Omniglot-$normal-$partition"
    sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
      scripts/main.sh resflow omniglot --normal $normal --anomalies "$partition" --device cuda "$@" &
  done
done

wait
