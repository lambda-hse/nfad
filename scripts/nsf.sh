#!/bin/bash

DATASETS='higgs susy kdd'

for DATASET in ${DATASETS}; do
  bash "scripts/nsf-${DATASET}.sh"
done
