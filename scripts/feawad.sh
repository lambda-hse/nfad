#!/bin/bash

DATASETS='higgs susy kdd'

for DATASET in ${DATASETS}; do
  bash "scripts/feawad-${DATASET}.sh"
done

