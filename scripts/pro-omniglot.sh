#!/bin/bash

mkdir -p logs/

python scripts/warmup.py omniglot || exit $?

for normal in 0 1 2; do
  for partition in 0 1 2 4; do
    FILENAME="PRO-Omniglot-$normal-$partition"
    sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
      scripts/main.sh pro omniglot --normal $normal --anomalies "$partition" --device cuda "$@" &
  done
done

wait

