#!/bin/bash

DATASETS='mnist cifar omniglot higgs susy kdd'

for DATASET in ${DATASETS}; do
  bash "scripts/pro-${DATASET}.sh"
done
