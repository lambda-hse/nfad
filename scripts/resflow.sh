#!/bin/bash

DATASETS='mnist cifar omniglot'

for DATASET in ${DATASETS}; do
  bash "scripts/resflow-${DATASET}.sh"
done