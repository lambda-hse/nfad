#!/bin/bash

DATASETS='mnist cifar omniglot higgs susy kdd'

for DATASET in ${DATASETS}; do
  bash "scripts/deepsad-${DATASET}.sh"
done
