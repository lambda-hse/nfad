#!/bin/bash

mkdir -p logs/

python scripts/warmup.py susy || exit $?

for partition in 0 100 1000 10000 1000000; do
  FILENAME="FEAWAD-SUSY-$partition"
  sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
    scripts/main.sh feawad susy --anomalies "$partition" --device cuda "$@" &
done

wait

