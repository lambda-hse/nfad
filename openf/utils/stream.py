__all__ = [
  'seq'
]

def seq(size, batch_size):
  n_batches = size // batch_size + (0 if size % batch_size == 0 else 1)

  for i in range(n_batches):
    from_indx = i * batch_size
    to_indx = min(from_indx + batch_size, size)

    yield slice(from_indx, to_indx)