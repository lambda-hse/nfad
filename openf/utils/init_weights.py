import torch
import torch.nn as nn

def init_weights(m):
    try:
        classname = m.__class__.__name__
        if any([classname.find(layer_name) != -1 for layer_name in ['Linear', 'Conv']]):
            torch.nn.init.xavier_uniform_(m.weight)
            if m.bias is not None:
                m.bias.data.fill_(0.01)

        elif classname.find('BatchNorm') != -1:
            if m.weight is not None:
                m.weight.data.normal_(1.0, 0.02)

            if m.bias is not None:
                m.bias.data.fill_(0)

    except torch.nn.modules.module.ModuleAttributeError:
        pass