# Code is taken from https://github.com/rtqichen/residual-flows

import os.path
import numpy as np
from types import SimpleNamespace

import torch
from resflow_utils import *
# from .resflow_utils import *
import lib.utils as utils




def main(train_loader, test_loader, **kwargs):
    args = {k: v for k, v in default_args.items()}
    for k,v in kwargs.items(): args[k] = v
    args = SimpleNamespace(**args)

    meter = build_meter()

    # Random seed
    if args.seed is None:
        args.seed = np.random.randint(100000)

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    fixed_z = standard_normal_sample([min(32, args.batchsize),
                                      (args.im_dim + args.padding) * args.imagesize * args.imagesize]).to(device)

    model, input_size, squeeze_layer = build_model(**kwargs)
    model.to(device)
    ema = utils.ExponentialMovingAverage(model)

    opt_pdf, opt_clf = build_optimizers(model, args)

    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if device.type == 'cuda':
        torch.cuda.manual_seed(args.seed)

    # for epoch in range(args.nepochs_nf):
        # train(epoch, model, squeeze_layer, input_size, ema, opt_pdf, opt_clf, train_loader, args, device, meter, fixed_z, 'density')


    # for epoch in range(args.nepochs_clf):
    #     train(epoch, model, squeeze_layer, input_size, ema, opt_pdf, opt_clf, train_loader, args, device, meter, fixed_z,
    #           'classification')

    # anomalies_cnt = 32
    # anomalies = sample(model, squeeze_layer, input_size, anomalies_cnt, args, device)
    # print(anomalies.shape)

    # y_pred = predict(model, squeeze_layer, input_size, test_loader, device, args)
    # print(y_pred)
    # print(y_pred.shape)

    return model, input_size, squeeze_layer

if __name__ == '__main__':
    import sys; sys.path.append('../../')
    from openf.datasets import available_datasets
    from openf.datasets.utils import combine
    import gc
    dataset_name = 'cifar'
    dataset = available_datasets[dataset_name](pos_class=0)
    n_anomalies = 1
    seed = 1
    batch_size=32
    X_normal, X_anomalous = dataset.partition(n_anomalies, seed=seed)
    data_train, labels_train = combine(X_normal, X_anomalous)
    del X_normal, X_anomalous; gc.collect()
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    X_train = torch.tensor(data_train, dtype=torch.float32, requires_grad=False)
    y_train = torch.tensor(labels_train, dtype=torch.float32, requires_grad=False)
    data_test, labels_test = dataset.data_test, dataset.labels_test
    X_test = torch.tensor(data_test, dtype=torch.float32, requires_grad=False)

    import torch.nn.functional as F
    image_size = 32
    X_train, X_test = [F.interpolate(x,image_size) for x in [X_train, X_test]]

    dataloader_train = torch.utils.data.DataLoader(
        torch.utils.data.TensorDataset(X_train, y_train),
        shuffle=True, batch_size=batch_size,
    )
    dataloader_test = torch.utils.data.DataLoader(torch.utils.data.TensorDataset(X_test), shuffle=False, batch_size=batch_size)

    # import cv2

    main(dataloader_train, dataloader_test, data=dataset_name, im_dim=3, imagesize=image_size, experiment_id=1)