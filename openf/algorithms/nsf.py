import numpy as np
import torch
import scipy.stats as st

from torch import optim
from torch.nn import functional as F
from torch.nn.utils import clip_grad_norm_
from torch.utils import data
from tqdm.auto import tqdm
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

import nn as nn_
import utils
from types import SimpleNamespace

from nde import distributions, flows, transforms

default_args = {
    'jacob_reg': 0,
    'train_batch_size': 100,
    'val_frac':1.,
    'val_batch_size':512,
    'learning_rate': 3e-4,
    'epoches': 3,
    'num_training_steps': 20000,
    'anneal_learning_rate': 1,
    'grad_norm_clip_value': 5.,
    'base_transform_type': 'rq-autoregressive',
    'linear_transform_type': 'lu',
    'num_flow_steps': 10,
    'hidden_features': 256,
    'tail_bound': 3,
    'num_bins': 8,
    'num_transform_blocks': 2,
    'use_batch_norm': 0,
    'dropout_probability': 0.25,
    'apply_unconditional_transform': 1,
    'monitor_interval': 250,
    'seed': 1638128
}


def create_linear_transform(n_features, args):
    if args.linear_transform_type == 'permutation':
        return transforms.RandomPermutation(features=n_features)
    elif args.linear_transform_type == 'lu':
        return transforms.CompositeTransform([
            transforms.RandomPermutation(features=n_features),
            transforms.LULinear(n_features, identity_init=True)
        ])
    elif args.linear_transform_type == 'svd':
        return transforms.CompositeTransform([
            transforms.RandomPermutation(features=features),
            transforms.SVDLinear(n_features, num_householder=10, identity_init=True)
        ])
    else:
        raise ValueError


def create_base_transform(i, features, args):
    if args.base_transform_type == 'affine-coupling':
        return transforms.AffineCouplingTransform(
            mask=utils.create_alternating_binary_mask(features, even=(i % 2 == 0)),
            transform_net_create_fn=lambda in_features, out_features: nn_.ResidualNet(
                in_features=in_features,
                out_features=out_features,
                hidden_features=args.hidden_features,
                context_features=None,
                num_blocks=args.num_transform_blocks,
                activation=F.relu,
                dropout_probability=args.dropout_probability,
                use_batch_norm=args.use_batch_norm
            )
        )
    elif args.base_transform_type == 'quadratic-coupling':
        return transforms.PiecewiseQuadraticCouplingTransform(
            mask=utils.create_alternating_binary_mask(features, even=(i % 2 == 0)),
            transform_net_create_fn=lambda in_features, out_features: nn_.ResidualNet(
                in_features=in_features,
                out_features=out_features,
                hidden_features=args.hidden_features,
                context_features=None,
                num_blocks=args.num_transform_blocks,
                activation=F.relu,
                dropout_probability=args.dropout_probability,
                use_batch_norm=args.use_batch_norm
            ),
            num_bins=args.num_bins,
            tails='linear',
            tail_bound=args.tail_bound,
            apply_unconditional_transform=args.apply_unconditional_transform
        )
    elif args.base_transform_type == 'rq-coupling':
        return transforms.PiecewiseRationalQuadraticCouplingTransform(
            mask=utils.create_alternating_binary_mask(features, even=(i % 2 == 0)),
            transform_net_create_fn=lambda in_features, out_features: nn_.ResidualNet(
                in_features=in_features,
                out_features=out_features,
                hidden_features=args.hidden_features,
                context_features=None,
                num_blocks=args.num_transform_blocks,
                activation=F.relu,
                dropout_probability=args.dropout_probability,
                use_batch_norm=args.use_batch_norm
            ),
            num_bins=args.num_bins,
            tails='linear',
            tail_bound=args.tail_bound,
            apply_unconditional_transform=args.apply_unconditional_transform
        )
    elif args.base_transform_type == 'affine-autoregressive':
        return transforms.MaskedAffineAutoregressiveTransform(
            features=features,
            hidden_features=args.hidden_features,
            context_features=None,
            num_blocks=args.num_transform_blocks,
            use_residual_blocks=True,
            random_mask=False,
            activation=F.relu,
            dropout_probability=args.dropout_probability,
            use_batch_norm=args.use_batch_norm
        )
    elif args.base_transform_type == 'quadratic-autoregressive':
        return transforms.MaskedPiecewiseQuadraticAutoregressiveTransform(
            features=features,
            hidden_features=args.hidden_features,
            context_features=None,
            num_bins=args.num_bins,
            tails='linear',
            tail_bound=args.tail_bound,
            num_blocks=args.num_transform_blocks,
            use_residual_blocks=True,
            random_mask=False,
            activation=F.relu,
            dropout_probability=args.dropout_probability,
            use_batch_norm=args.use_batch_norm
        )
    elif args.base_transform_type == 'rq-autoregressive':
        return transforms.MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
            features=features,
            hidden_features=args.hidden_features,
            context_features=None,
            num_bins=args.num_bins,
            tails='linear',
            tail_bound=args.tail_bound,
            num_blocks=args.num_transform_blocks,
            use_residual_blocks=True,
            random_mask=False,
            activation=F.relu,
            dropout_probability=args.dropout_probability,
            use_batch_norm=args.use_batch_norm
        )
    else:
        raise ValueError


def create_transform(n_features:int, args):
    transform = transforms.CompositeTransform([
        transforms.CompositeTransform([
            create_linear_transform(n_features, args),
            create_base_transform(i, n_features, args)
        ]) for i in range(args.num_flow_steps)
    ] + [
        create_linear_transform(n_features, args)
    ])
    return transform

def batch_generator(loader, num_batches=int(1e10)):
    batch_counter = 0
    while True:
        for batch in loader:
            yield batch
            batch_counter += 1
            if batch_counter == num_batches:
                return

def randn_tail(shape, from_=2,to_=11):
    size = shape[0]
    half_size = size//2
    return(torch.from_numpy(np.vstack([st.truncnorm.rvs(-to_, -from_, size=(half_size,)+shape[1:]),
               st.truncnorm.rvs(from_, to_, size=(size-half_size,)+shape[1:])])))

def sample(nf, shape, device, tail=True):
    if tail:
        fixed_z = randn_tail(shape).to(device)
    else:
        fixed_z = torch.randn(shape).to(device)
    with torch.no_grad():
        samples, _ = nf.transform.inverse(fixed_z.float())
    return samples

def build_model(n_features:int, **kwargs):
    args = {k:v for k,v in default_args.items()}
    for k,v in kwargs.items(): args[k] = v
    args = SimpleNamespace(**args)

    distribution = distributions.StandardNormal((n_features,))
    transform = create_transform(n_features, args)
    flow = flows.Flow(transform, distribution)
    return flow

def train_nf(flow, X_pos, **kwargs):
    args = {k:v for k,v in default_args.items()}
    for k,v in kwargs.items(): args[k] = v
    args = SimpleNamespace(**args)

    X_train = torch.tensor(X_pos, dtype=torch.float32, requires_grad=False)

    dataloader = torch.utils.data.DataLoader(
        torch.utils.data.TensorDataset(X_train),
        shuffle=True, batch_size=args.train_batch_size,
    )

    torch.manual_seed(args.seed)
    np.random.seed(args.seed)

    if torch.cuda.is_available():
        device = torch.device('cuda')
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
    else: device = torch.device('cpu')

    # # create data
    # train_generator = batch_generator(dataloader)

    # create optimizer
    optimizer = optim.Adam(flow.parameters(), lr=args.learning_rate, weight_decay=1e-5)
    if args.anneal_learning_rate:
        scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, args.num_training_steps, 0)
    else:
        scheduler = None

    # train
    tbar = tqdm(range(args.epoches))
    losses = []
    print('Training started!')

    flow.train()
    for epoch in tbar:
        for batch_X in dataloader:
            batch_X = batch_X[0]
            if args.anneal_learning_rate:
                scheduler.step()
            optimizer.zero_grad()

            batch_X = batch_X.to(device).float()
            log_density, logabsdet = flow.log_prob(batch_X, return_logdet=True) # in order to perform Jacobian regularization
            loss = torch.mean(-log_density + args.jacob_reg * (logabsdet - 1)**2) # Train on normal samples only (batch_y = 1)
            loss.backward()
            if args.grad_norm_clip_value is not None:
                clip_grad_norm_(flow.parameters(), args.grad_norm_clip_value)
            optimizer.step()
        losses.append(loss.detach().cpu().item())
    return losses

if __name__ == '__main__':
    import sys; sys.path.append('../../')
    from openf.datasets import available_datasets
    from openf.datasets.utils import combine
    import gc
    dataset_name = 'higgs'
    dataset = available_datasets[dataset_name]()
    n_anomalies = 1
    seed = 1
    batch_size=32
    X_normal, X_anomalous = dataset.partition(n_anomalies, seed=seed)
    data_train, labels_train = combine(X_normal, X_anomalous)
    del X_normal, X_anomalous; gc.collect()
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    X_train = torch.tensor(data_train, dtype=torch.float32, requires_grad=False)
    y_train = torch.tensor(labels_train, dtype=torch.float32, requires_grad=False)
    data_test, labels_test = dataset.data_test, dataset.labels_test
    X_test = torch.tensor(data_test, dtype=torch.float32, requires_grad=False)

    dataloader_train = torch.utils.data.DataLoader(
        torch.utils.data.TensorDataset(X_train, y_train),
        shuffle=True, batch_size=batch_size,
    )

    # import cv2

    # nf = build_model(X_train.shape[1], args, device)
    #
    # train_nf(nf, dataloader_train, num_training_steps=1)
    #
    # anomalies_cnt = 10
    # anomalies = sample(nf, (anomalies_cnt, X_train.shape[1]), device)

