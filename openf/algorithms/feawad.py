# -*- coding: utf-8 -*- 
"""
@author：Xucheng Song
The algorithm was implemented using Python 3.6.12, Keras 2.3.1 and TensorFlow 1.13.1 based on the code (https://github.com/GuansongPang/deviation-network).
The major contributions are summarized as follows. 

This code adds a feature encoder to encode the input data and utilizes three factors, hidden representation, reconstruction residual vector,
and reconstruction error, as the new representation for the input data. The representation is then fed into an MLP based anomaly score generator,
similar to the code (https://github.com/GuansongPang/deviation-network), but with a twist, i.e., the reconstruction error is fed to each layer
of the MLP in the anomaly score generator. A different loss function in the anomaly score generator is also included. Additionally,
the pre-training procedure is adopted in the training process. More details can be found in our TNNLS paper as follows.

Yingjie Zhou, Xucheng Song, Yanru Zhang, Fanxing Liu, Ce Zhu and Lingqiao Liu,
Feature Encoding with AutoEncoders for Weakly-supervised Anomaly Detection,
in IEEE Transactions on Neural Networks and Learning Systems, 2021, 12 pages,
which can be found in IEEE Xplore or arxiv (https://arxiv.org/abs/2105.10500).
"""

import numpy as np
np.random.seed(42)
import tensorflow as tf
from tensorflow.python.framework.ops import disable_eager_execution
disable_eager_execution()

#tf.set_random_seed(42)
#sess = tf.Session()

from tensorflow.keras import backend as K

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, Subtract,concatenate,Lambda,Reshape
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.losses import mean_squared_error

import argparse
from types import SimpleNamespace

import numpy as np
import sys
from scipy.sparse import vstack, csc_matrix
from sklearn.model_selection import train_test_split

import numpy as np
from sklearn.metrics import average_precision_score, roc_auc_score
# from sklearn.externals.joblib import Memory
from sklearn.datasets import load_svmlight_file
import csv

# mem = Memory("./dataset/")

# @mem.cache
# def get_data_from_svmlight_file(path):
#     data = load_svmlight_file(path)
#     return data[0], data[1]

def dataLoading(path, byte_num):
    # loading data
    x=[]
    labels=[]
    
    with (open(path,'r')) as data_from:
        csv_reader=csv.reader(data_from)
        for i in csv_reader:
            x.append(i[0:byte_num])
            labels.append(i[byte_num])

    for i in range(len(x)):
        for j in range(byte_num):
            x[i][j] = float(x[i][j])
    for i in range(len(labels)):
        labels[i] = float(labels[i])
    x = np.array(x)
    labels = np.array(labels)

    return x, labels;


def aucPerformance(mse, labels):
    roc_auc = roc_auc_score(labels, mse)
    ap = average_precision_score(labels, mse)
    print("AUC-ROC: %.4f, AUC-PR: %.4f" % (roc_auc, ap))
    return roc_auc, ap;

def writeResults(name, n_samples_trn,  n_outliers, n_samples_test,test_outliers ,test_inliers, avg_AUC_ROC, avg_AUC_PR, std_AUC_ROC,std_AUC_PR, path):    
    csv_file = open(path, 'a') 
    row = name + ","  + n_samples_trn + ','+n_outliers  + ','+n_samples_test+','+test_outliers+','+test_inliers+','+avg_AUC_ROC+','+avg_AUC_PR+','+std_AUC_ROC+','+std_AUC_PR + "\n"
    csv_file.write(row)

MAX_INT = np.iinfo(np.int32).max
data_format = 0

default_args = {
    "network_depth": '4',
    "batch_size": 512,
    "nb_batch": 20,
    "epochs": 30,
    "runs": 10,
    "known_outliers": 30,
    "cont_rate": 0.02,
    "input_path": './dataset/',
    "data_set": 'nslkdd_normalization',
    "data_format": '0',
    "data_dim": 122,
    "output": './proposed_devnet_auc_performance.csv',
    "ramdn_seed": 42
    }

def get_args(**kwargs):
    args = {k: v for k, v in default_args.items()}
    for k, v in kwargs.items(): args[k] = v
    args = SimpleNamespace(**args)
    return args


def auto_encoder(input_shape):
    x_input = Input(shape=input_shape)
    length = K.int_shape(x_input)[1]
  
    input_vector = Dense(length, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'ain')(x_input)     
    en1 = Dense(128, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'ae1')(input_vector)   
    en2 = Dense(64,kernel_initializer='glorot_normal', use_bias=True,activation='relu',name = 'ae2')(en1)
    de1 = Dense(128, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'ad1')(en2)
    de2 = Dense(length, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'ad2')(de1)

    model =  Model(x_input, de2)
    adm = Adam(lr=0.0001)
    model.compile(loss=mean_squared_error, optimizer=adm, experimental_run_tf_function=False)
    return model

def dev_network_d(input_shape,modelname,testflag):
    '''
    deeper network architecture with three hidden layers
    '''
    x_input = Input(shape=input_shape)
    length = K.int_shape(x_input)[1]
  
    input_vector = Dense(length, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'ain')(x_input)     
    en1 = Dense(128, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'ae1')(input_vector)   
    en2 = Dense(64,kernel_initializer='glorot_normal', use_bias=True,activation='relu',name = 'ae2')(en1)
    de1 = Dense(128, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'ad1')(en2)
    de2 = Dense(length, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'ad2')(de1)
    
    if testflag==0:
        AEmodel = Model(x_input,de2)
        AEmodel.load_weights(modelname)
        print('load autoencoder model')

        sub_result = Subtract()([x_input, de2])
        cal_norm2 = Lambda(lambda x: tf.norm(x,ord = 2,axis=1))
        sub_norm2 = cal_norm2(sub_result)
        sub_norm2 = Reshape((1,))(sub_norm2)
        division = Lambda(lambda x:tf.math.divide(x[0],x[1]))
        sub_result = division([sub_result,sub_norm2])
        conca_tensor = concatenate([sub_result,en2],axis=1)

        conca_tensor = concatenate([conca_tensor,sub_norm2],axis=1)
    else:
        sub_result = Subtract()([x_input, de2])
        cal_norm2 = Lambda(lambda x: tf.norm(x,ord = 2,axis=1))
        sub_norm2 = cal_norm2(sub_result)
        sub_norm2 = Reshape((1,))(sub_norm2)
        division = Lambda(lambda x:tf.math.divide(x[0],x[1]))
        sub_result = division([sub_result,sub_norm2])
        conca_tensor = concatenate([sub_result,en2],axis=1)

        conca_tensor = concatenate([conca_tensor,sub_norm2],axis=1)

    intermediate = Dense(256, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'hl2')(conca_tensor)
    intermediate = concatenate([intermediate,sub_norm2],axis=1)
    intermediate = Dense(32, kernel_initializer='glorot_normal',use_bias=True,activation='relu',name = 'hl3')(intermediate)
    intermediate = concatenate([intermediate,sub_norm2],axis=1)
    output_pre = Dense(1, kernel_initializer='glorot_normal',use_bias=True,activation='linear', name = 'score')(intermediate)
    dev_model = Model(x_input, output_pre)
    def multi_loss(y_true,y_pred):
        y_true = tf.cast(y_true, tf.float32)
        confidence_margin = 5. 

        dev = y_pred
        inlier_loss = K.abs(dev)
        outlier_loss = K.abs(K.maximum(confidence_margin - dev, 0.))

        sub_nor = tf.norm(sub_result,ord = 2,axis=1) 
        outlier_sub_loss = K.abs(K.maximum(confidence_margin - sub_nor, 0.))
        loss1 =  (1 - y_true) * (inlier_loss+sub_nor) + y_true * (outlier_loss+outlier_sub_loss)

        return loss1

    adm = Adam(lr=0.0001)
    dev_model.compile(loss=multi_loss, optimizer=adm, experimental_run_tf_function=False)
    return dev_model

def deviation_network(input_shape, network_depth,modelname,testflag):
    '''
    construct the deviation network-based detection model
    '''
    if network_depth == 4:
        model = dev_network_d(input_shape,modelname,testflag)
    elif network_depth == 2:
        model = auto_encoder(input_shape)

    else:
        sys.exit("The network depth is not set properly")
    return model

def auto_encoder_batch_generator_sup(x,inlier_indices, batch_size, nb_batch, rng):
    """auto encoder batch generator
    """
    rng = np.random.RandomState(rng.randint(MAX_INT, size = 1))
    counter = 0
    while 1:
        if data_format == 0:
            ref, training_labels = AE_input_batch_generation_sup(x, inlier_indices,batch_size, rng)
        else:
            ref, training_labels = input_batch_generation_sup_sparse(x, inlier_indices,batch_size, rng)
        counter += 1
        yield(ref, training_labels)
        if (counter > nb_batch):
            counter = 0
def AE_input_batch_generation_sup(train_x,inlier_indices, batch_size, rng):
    '''
    batchs of samples. This is for csv data.
    Alternates between positive and negative pairs.
    '''      
    dim = train_x.shape[1]
    ref = np.empty((batch_size, dim))
    training_labels = np.empty((batch_size, dim)) 
    n_inliers = len(inlier_indices)
    for i in range(batch_size):
        sid = rng.choice(n_inliers, 1)
        ref[i] = train_x[inlier_indices[sid]]
        training_labels[i] = train_x[inlier_indices[sid]]
    return np.array(ref), np.array(training_labels)
def batch_generator_sup(x, outlier_indices, inlier_indices, batch_size, nb_batch, rng):
    """batch generator
    """
    rng = np.random.RandomState(rng.randint(MAX_INT, size = 1))
    counter = 0
    while 1:
        if data_format == 0:
            ref, training_labels = input_batch_generation_sup(x, outlier_indices, inlier_indices, batch_size, rng)
        else:
            ref, training_labels = input_batch_generation_sup_sparse(x, outlier_indices, inlier_indices, batch_size, rng)
        counter += 1
        yield(ref, training_labels)
        if (counter > nb_batch):
            counter = 0
 
def input_batch_generation_sup(train_x, outlier_indices, inlier_indices, batch_size, rng):
    '''
    batchs of samples. This is for csv data.
    Alternates between positive and negative pairs.
    '''      
    dim = train_x.shape[1]
    ref = np.empty((batch_size, dim))
    training_labels = []
    n_inliers = len(inlier_indices)
    n_outliers = len(outlier_indices)
    for i in range(batch_size):
        if(i % 2 == 0):   
            sid = rng.choice(n_inliers, 1)
            ref[i] = train_x[inlier_indices[sid]]
            training_labels += [0]
        else:
            sid = rng.choice(n_outliers, 1)
            ref[i] = train_x[outlier_indices[sid]]
            training_labels += [1]
    return np.array(ref), np.array(training_labels)

def input_batch_generation_sup_sparse(train_x, outlier_indices, inlier_indices, batch_size, rng):
    '''
    batchs of samples. This is for libsvm stored sparse data.
    Alternates between positive and negative pairs.
    '''      
    ref = np.empty((batch_size))    
    training_labels = []
    n_inliers = len(inlier_indices)
    n_outliers = len(outlier_indices)
    for i in range(batch_size):
        if(i % 2 == 0):
            sid = rng.choice(n_inliers, 1)
            ref[i] = inlier_indices[sid]
            training_labels += [0]
        else:
            sid = rng.choice(n_outliers, 1)
            ref[i] = outlier_indices[sid]
            training_labels += [1]
    ref = train_x[ref, :].toarray()
    return ref, np.array(training_labels)

def load_model_weight_predict(model_name, input_shape, network_depth, test_x):
    '''
    load the saved weights to make predictions
    '''
    model = deviation_network(input_shape, network_depth,model_name,1)
    model.load_weights(model_name)
    scoring_network = Model(inputs=model.input, outputs=model.output)
    
    if data_format == 0:
        scores = scoring_network.predict(test_x)
    else:
        data_size = test_x.shape[0]
        scores = np.zeros([data_size, 1])
        count = 512
        i = 0
        while i < data_size:
            subset = test_x[i:count].toarray()
            scores[i:count] = scoring_network.predict(subset)
            if i % 1024 == 0:
                print(i)
            i = count
            count += 512
            if count > data_size:
                count = data_size
        assert count == data_size
    return scores

def inject_noise_sparse(seed, n_out, random_seed):  
    '''
    add anomalies to training data to replicate anomaly contaminated data sets.
    we randomly swape 5% features of anomalies to avoid duplicate contaminated anomalies.
    This is for sparse data.
    '''
    rng = np.random.RandomState(random_seed) 
    n_sample, dim = seed.shape
    swap_ratio = 0.05
    n_swap_feat = int(swap_ratio * dim)
    seed = seed.tocsc()
    noise = csc_matrix((n_out, dim))
    print(noise.shape)
    for i in np.arange(n_out):
        outlier_idx = rng.choice(n_sample, 2, replace = False)
        o1 = seed[outlier_idx[0]]
        o2 = seed[outlier_idx[1]]
        swap_feats = rng.choice(dim, n_swap_feat, replace = False)
        noise[i] = o1.copy()
        noise[i, swap_feats] = o2[0, swap_feats]
    return noise.tocsr()

def inject_noise(seed, n_out, random_seed):   
    '''
    add anomalies to training data to replicate anomaly contaminated data sets.
    we randomly swape 5% features of anomalies to avoid duplicate contaminated anomalies.
    this is for dense data
    ''' 
    rng = np.random.RandomState(random_seed) 
    n_sample, dim = seed.shape
    swap_ratio = 0.05
    n_swap_feat = int(swap_ratio * dim)
    noise = np.empty((n_out, dim))
    for i in np.arange(n_out):
        outlier_idx = rng.choice(n_sample, 2, replace = False)
        o1 = seed[outlier_idx[0]]
        o2 = seed[outlier_idx[1]]
        swap_feats = rng.choice(dim, n_swap_feat, replace = False)
        noise[i] = o1.copy()
        noise[i, swap_feats] = o2[swap_feats]
    return noise

if __name__ == '__main__':
    run_devnet(args)
