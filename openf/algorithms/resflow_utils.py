# Code is taken from https://github.com/rtqichen/residual-flows

import math
import time
import gc
import os

import numpy as np
import scipy.stats as st
import torch
from types import SimpleNamespace

import sys; sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from lib.resflow import ACT_FNS, ResidualFlow
import lib.optimizers as optim
import lib.layers as layers
import lib.layers.base as base_layers
import lib.utils as utils
from lib.lr_scheduler import CosineAnnealingWarmRestarts
from typing import Optional
import torch.nn.functional as F
from tqdm import tqdm
from torchvision.utils import save_image
from itertools import chain


default_args = {'dataroot': 'data',
                'imagesize': 32,
                'jac_reg': 0,
                'nbits': 8,
                'block': 'resblock',
                'coeff': 0.98,
                'vnorms': '2222',
                'n_lipschitz_iters': None,
                'sn_tol': 1e-3,
                'learn_p': False,
                'n_power_series': None,
                'factor_out': False,
                'n_dist': 'poisson',
                'n_samples': 1,
                'n_exact_terms': 2,
                'var_reduc_lr': 0,
                'neumann_grad': True,
                'mem_eff': True,
                'act': 'swish',
                'idim': 512,
                'nblocks': '16-16-16',
                'squeeze_first': False,
                'actnorm': True,
                'fc_actnorm': False,
                'batchnorm': False,
                'dropout': 0.,
                'fc': False,
                'kernels': '3-1-3',
                'add_noise': True,
                'quadratic': False,
                'fc_end': True,
                'fc_idim': 128,
                'preact': True,
                'padding': 0,
                'first_resblock': True,
                'cdim': 256,
                'optimizer': 'adam',
                'scheduler': False,
                'nepochs_nf': 15,
                'nepochs_clf': 8,
                'batchsize': 40,
                'lr': 1e-3,
                'wd': 0,
                'warmup_iters': 1000,
                'annealing_iters': 0,
                'save': 'experiment1',
                'val_batchsize': 200,
                'seed': None,
                'ema_val': True,
                'update_freq': 1,
                'scale_dim': False,
                'rcrop_pad_mode': 'reflect',
                'padding_dist': 'uniform',
                'resume': None,
                'begin_epoch': 0,
                'nworkers': 4,
                'print_freq': 20,
                'vis_freq': 100,
                'visualize': False,
                # 'im_dim': 3, # Channels count. 3 for RGB, 1 for B/W
                'n_classes': 2, # anomaly or not
                'parallelize': False
                }

def get_args(**kwargs):
    args = {k: v for k, v in default_args.items()}
    for k, v in kwargs.items(): args[k] = v
    args = SimpleNamespace(**args)
    return args

def build_meter():
    return SimpleNamespace(batch_time=utils.RunningAverageMeter(0.97),
                    bpd_meter=utils.RunningAverageMeter(0.97),
                    logpz_meter=utils.RunningAverageMeter(0.97),
                    deltalogp_meter=utils.RunningAverageMeter(0.97),
                    firmom_meter=utils.RunningAverageMeter(0.97),
                    secmom_meter=utils.RunningAverageMeter(0.97),
                    gnorm_meter=utils.RunningAverageMeter(0.97),
                    ce_meter=utils.RunningAverageMeter(0.97))

def geometric_logprob(ns, p):
    return torch.log(1 - p + 1e-10) * (ns - 1) + torch.log(p + 1e-10)

def standard_normal_sample(size):
    return torch.randn(size)


def standard_normal_logprob(z):
    logZ = -0.5 * math.log(2 * math.pi)
    return logZ - z.pow(2) / 2

def randn_tail(shape, upper_bound_logpdf, one_step_samples=None):
    samples = np.random.randn(0,*shape[1:])
    size = shape[0]
    if one_step_samples is None:
        one_step_samples = shape[0]
    while samples.shape[0] < size:
        new_samples = torch.randn(one_step_samples, *shape[1:])
        pdf = standard_normal_logprob(new_samples.reshape(one_step_samples, -1)).sum(1)
        new_samples = new_samples[pdf < upper_bound_logpdf]
        samples = np.concatenate([samples, new_samples])
    samples = samples[:size]
    return torch.from_numpy(samples)


def normal_logprob(z, mean, log_std):
    mean = mean + torch.tensor(0.)
    log_std = log_std + torch.tensor(0.)
    c = torch.tensor([math.log(2 * math.pi)]).to(z)
    inv_sigma = torch.exp(-log_std)
    tmp = (z - mean) * inv_sigma
    return -0.5 * (tmp * tmp + 2 * log_std + c)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def reduce_bits(x, nbits):
    if nbits < 8:
        x = x * 255
        x = torch.floor(x / 2**(8 - nbits))
        x = x / 2**nbits
    return x


def add_noise(x, args, nvals=256):
    """
    [0, 1] -> [0, nvals] -> add noise -> [0, 1]
    """
    if args.add_noise:
        noise = x.new().resize_as_(x).uniform_()
        x = x * (nvals - 1) + noise
        x = x / nvals
    return x


def update_lr(optimizer, itr, args):
    iter_frac = min(float(itr + 1) / max(args.warmup_iters, 1), 1.0)
    lr = args.lr * iter_frac
    for param_group in optimizer.param_groups:
        param_group["lr"] = lr


def add_padding(x, args, nvals=256):
    # Theoretically, padding should've been added before the add_noise preprocessing.
    # nvals takes into account the preprocessing before padding is added.
    if args.padding > 0:
        if args.padding_dist == 'uniform':
            u = x.new_empty(x.shape[0], args.padding, x.shape[2], x.shape[3]).uniform_()
            logpu = torch.zeros_like(u).sum([1, 2, 3]).view(-1, 1)
            return torch.cat([x, u / nvals], dim=1), logpu
        elif args.padding_dist == 'gaussian':
            u = x.new_empty(x.shape[0], args.padding, x.shape[2], x.shape[3]).normal_(nvals / 2, nvals / 8)
            logpu = normal_logprob(u, nvals / 2, math.log(nvals / 8)).sum([1, 2, 3]).view(-1, 1)
            return torch.cat([x, u / nvals], dim=1), logpu
        else:
            raise ValueError()
    else:
        return x, torch.zeros(x.shape[0], 1).to(x)


def remove_padding(x, args):
    if args.padding > 0:
        return x[:, :args.im_dim, :, :]
    else:
        return x

def build_model(**kwargs):
    args = get_args(**kwargs)

    input_size = (args.batchsize, args.im_dim + args.padding, args.imagesize, args.imagesize)
    squeeze_layer = None
    if args.squeeze_first:
        input_size = (input_size[0], input_size[1] * 4, input_size[2] // 2, input_size[3] // 2)
        squeeze_layer = layers.SqueezeLayer(2)

    # Dataset and hyperparameters
    if args.data == 'cifar':
        init_layer = layers.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010))
    else:
        init_layer = layers.LogitTransform(1e-6)

    # Model
    model = ResidualFlow(
        input_size,
        n_blocks=list(map(int, args.nblocks.split('-'))),
        intermediate_dim=args.idim,
        factor_out=args.factor_out,
        quadratic=args.quadratic,
        init_layer=init_layer,
        actnorm=args.actnorm,
        fc_actnorm=args.fc_actnorm,
        batchnorm=args.batchnorm,
        dropout=args.dropout,
        fc=args.fc,
        coeff=args.coeff,
        vnorms=args.vnorms,
        n_lipschitz_iters=args.n_lipschitz_iters,
        sn_atol=args.sn_tol,
        sn_rtol=args.sn_tol,
        n_power_series=args.n_power_series,
        n_dist=args.n_dist,
        n_samples=args.n_samples,
        kernels=args.kernels,
        activation_fn=args.act,
        fc_end=args.fc_end,
        fc_idim=args.fc_idim,
        n_exact_terms=args.n_exact_terms,
        preact=args.preact,
        neumann_grad=args.neumann_grad,
        grad_in_forward=args.mem_eff,
        first_resblock=args.first_resblock,
        learn_p=args.learn_p,
        classification=True,
        classification_hdim=args.cdim,
        n_classes=args.n_classes,
        block_type=args.block,
    )
    return model, input_size, squeeze_layer

def parallelize(model):
    return torch.nn.DataParallel(model)

# Optimization
def tensor_in(t, a):
    for a_ in a:
        if t is a_:
            return True
    return False

def build_optimizers(model, args):
    pdf_params, clf_params = model.transforms.parameters(), chain(model.classification_heads.parameters(), model.logit_layer.parameters())
    res = []
    for params in [pdf_params, clf_params]:
        scheduler = None
        if args.optimizer == 'adam':
            optimizer = optim.Adam(params, lr=args.lr, betas=(0.9, 0.99), weight_decay=args.wd)
            if args.scheduler: scheduler = CosineAnnealingWarmRestarts(optimizer, 20, T_mult=2, last_epoch=args.begin_epoch - 1)
        elif args.optimizer == 'adamax':
            optimizer = optim.Adamax(params, lr=args.lr, betas=(0.9, 0.99), weight_decay=args.wd)
        elif args.optimizer == 'rmsprop':
            optimizer = optim.RMSprop(params, lr=args.lr, weight_decay=args.wd)
        elif args.optimizer == 'sgd':
            optimizer = torch.optim.SGD(params, lr=args.lr, momentum=0.9, weight_decay=args.wd)
            if args.scheduler:
                scheduler = torch.optim.lr_scheduler.MultiStepLR(
                    optimizer, milestones=[60, 120, 160], gamma=0.2, last_epoch=args.begin_epoch - 1
                )
        else:
            raise ValueError('Unknown optimizer {}'.format(args.optimizer))
        res.append(optimizer)
    return res


def compute_loss(x, model, squeeze_layer, input_size:int, args, beta=1.0):

    if args.data == 'celeba_5bit':
        nvals = 32
    elif args.data == 'celebahq':
        nvals = 2**args.nbits
    else:
        nvals = 256

    x, logpu = add_padding(x, args, nvals)

    if args.squeeze_first:
        x = squeeze_layer(x)

    z_logp, logits_tensor = model(x.view(-1, *input_size[1:]), 0, classify=True)
    z, delta_logp = z_logp

    logpz = standard_normal_logprob(z).view(z.size(0), -1).sum(1, keepdim=True)

    # log p(x)
    logpx = logpz - beta * delta_logp - np.log(nvals) * (
        args.imagesize * args.imagesize * (args.im_dim + args.padding)
    ) - logpu

    logpz = torch.mean(logpz).detach()

    return logpx, logits_tensor, logpz, delta_logp


def estimator_moments(model):
    avg_first_moment = 0.
    avg_second_moment = 0.
    for m in model.modules():
        if isinstance(m, layers.iResBlock):
            avg_first_moment += m.last_firmom.item()
            avg_second_moment += m.last_secmom.item()
    return avg_first_moment, avg_second_moment


def compute_p_grads(model):
    scales = 0.
    nlayers = 0
    for m in model.modules():
        if isinstance(m, base_layers.InducedNormConv2d) or isinstance(m, base_layers.InducedNormLinear):
            scales = scales + m.compute_one_iter()
            nlayers += 1
    scales.mul(1 / nlayers).backward()
    for m in model.modules():
        if isinstance(m, base_layers.InducedNormConv2d) or isinstance(m, base_layers.InducedNormLinear):
            if m.domain.grad is not None and torch.isnan(m.domain.grad):
                m.domain.grad = None


def get_lipschitz_constants(model):
    lipschitz_constants = []
    for m in model.modules():
        if isinstance(m, base_layers.SpectralNormConv2d) or isinstance(m, base_layers.SpectralNormLinear):
            lipschitz_constants.append(m.scale)
        if isinstance(m, base_layers.InducedNormConv2d) or isinstance(m, base_layers.InducedNormLinear):
            lipschitz_constants.append(m.scale)
        if isinstance(m, base_layers.LopConv2d) or isinstance(m, base_layers.LopLinear):
            lipschitz_constants.append(m.scale)
    return lipschitz_constants


def update_lipschitz(model):
    with torch.no_grad():
        for m in model.modules():
            if isinstance(m, base_layers.SpectralNormConv2d) or isinstance(m, base_layers.SpectralNormLinear):
                m.compute_weight(update=True)
            if isinstance(m, base_layers.InducedNormConv2d) or isinstance(m, base_layers.InducedNormLinear):
                m.compute_weight(update=True)


def get_ords(model):
    ords = []
    for m in model.modules():
        if isinstance(m, base_layers.InducedNormConv2d) or isinstance(m, base_layers.InducedNormLinear):
            domain, codomain = m.compute_domain_codomain()
            if torch.is_tensor(domain):
                domain = domain.item()
            if torch.is_tensor(codomain):
                codomain = codomain.item()
            ords.append(domain)
            ords.append(codomain)
    return ords


def pretty_repr(a):
    return '[[' + ','.join(list(map(lambda i: f'{i:.2f}', a))) + ']]'

def sample(model, squeeze_layer, input_size, size, args, device, upper_bound_logpz=None):
    if upper_bound_logpz is None: 
        tail = False
    else:
        tail = True
    if tail:
        fixed_z = randn_tail((size,
                    (args.im_dim + args.padding) * args.imagesize * args.imagesize), upper_bound_logpz).to(device)
    else:
        fixed_z = standard_normal_sample([size,
                                          (args.im_dim + args.padding) * args.imagesize * args.imagesize]).to(device)
    with torch.no_grad():
        samples = model(fixed_z.float(), inverse=True).view(-1, *input_size[1:])
        if args.squeeze_first: samples = squeeze_layer.inverse(samples)
    return samples

def predict(model, squeeze_layer, input_size, test_loader, device, args):
    preds = []
    with torch.no_grad():
        if args.data == 'celeba_5bit':
            nvals = 32
        elif args.data == 'celebahq':
            nvals = 2 ** args.nbits
        else:
            nvals = 256

        for i, batch_X in enumerate(test_loader):
            batch_X = batch_X[0].to(device)

            x, logpu = add_padding(batch_X, args, nvals)

            if args.squeeze_first:
                x = squeeze_layer(x)

            # if task == 'hybrid':
            _, logits_tensor = model(x.view(-1, *input_size[1:]), 0, classify=True)
            preds.append(logits_tensor.detach().cpu().numpy().squeeze())
    return np.nan_to_num(np.concatenate(preds), 0)

def train(epoch, model, squeeze_layer, input_size, ema, opt_pdf, opt_clf, train_loader,
          args, device, meter, task, anomalies_log_pdf=-1000):
    if args.parallelize:
        model = parallelize(model)

    model.train()

    total = 0
    correct = 0

    end = time.time()

    opts = {'density': [opt_pdf], 'classification': [opt_clf], 'hybrid': [opt_clf, opt_pdf]}

    for i, (x, y) in enumerate(train_loader):

        global_itr = epoch * len(train_loader) + i
        for opt in opts[task]:
            update_lr(opt, global_itr, args)

        [x,y] = [x_.to(device) for x_ in [x,y]]

        beta = beta = min(1, global_itr / args.annealing_iters) if args.annealing_iters > 0 else 1.
        logpx, logits, logpz, delta_logp = compute_loss(x, model, squeeze_layer, input_size, args, beta=beta)

        bpd = -torch.mean(logpx*y) / (args.imagesize * args.imagesize * args.im_dim) / np.log(2) # normal_only

        if task in ['density', 'hybrid']:
            firmom, secmom = estimator_moments(model)

            meter.bpd_meter.update(bpd.item())
            meter.logpz_meter.update(logpz.item())
            meter.deltalogp_meter.update(delta_logp.mean().item())
            meter.firmom_meter.update(firmom)
            meter.secmom_meter.update(secmom)

        if task in ['classification', 'hybrid']:
            y = y.to(device).long()
            crossent = F.cross_entropy(logits, y)
            meter.ce_meter.update(crossent.item())

            # Compute accuracy.
            _, predicted = logits.max(1)
            total += y.size(0)
            correct += predicted.eq(y).sum().item()

        # compute gradient and do SGD step
        if task == 'density':
            loss = bpd + args.jac_reg*((delta_logp-1)**2).mean() # 2nd term is Jacobian regularization
        elif task == 'classification':
            loss = crossent
        else:
            if not args.scale_dim: bpd = bpd * (args.imagesize * args.imagesize * args.im_dim)
            loss = bpd + crossent / np.log(2)  # Change cross entropy from nats to bits.
        loss.backward()

        if task == 'classification':
            batch_size = x.shape[0]
            anomalies = sample(model, squeeze_layer, input_size, batch_size, args, device, anomalies_log_pdf)
            y = torch.zeros(batch_size).to(device).long()
            _, logits, _, _ = compute_loss(anomalies, model, squeeze_layer, input_size, args, beta=beta)
            loss = F.cross_entropy(logits, y)
            loss.backward()

        if global_itr % args.update_freq == args.update_freq - 1:

            if args.update_freq > 1:
                with torch.no_grad():
                    for p in model.parameters():
                        if p.grad is not None:
                            p.grad /= args.update_freq

            grad_norm = torch.nn.utils.clip_grad.clip_grad_norm_(model.parameters(), 1.)
            if args.learn_p: compute_p_grads(model)

            for opt in opts[task]:
                opt.step()

            opt_pdf.zero_grad()
            opt_clf.zero_grad()

            update_lipschitz(model)
            ema.apply()

            meter.gnorm_meter.update(grad_norm)

        # measure elapsed time
        meter.batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            s = (
                'Epoch: [{0}][{1}/{2}] | Time {batch_time.val:.3f} | '
                'GradNorm {gnorm_meter.avg:.2f}'.format(
                    epoch, i, len(train_loader), batch_time=meter.batch_time, gnorm_meter=meter.gnorm_meter
                )
            )

            if task in ['density', 'hybrid']:
                s += (
                    ' | Bits/dim {bpd_meter.val:.4f}({bpd_meter.avg:.4f}) | '
                    'Logpz {logpz_meter.avg:.0f} | '
                    'DeltaLogp {deltalogp_meter.avg:.0f} | '
                    'EstMoment ({firmom_meter.avg:.0f},{secmom_meter.avg:.0f})'.format(
                        bpd_meter=meter.bpd_meter, logpz_meter=meter.logpz_meter, deltalogp_meter=meter.deltalogp_meter,
                        firmom_meter=meter.firmom_meter, secmom_meter=meter.secmom_meter
                    )
                )

            if task in ['classification', 'hybrid']:
                s += ' | CE {ce_meter.avg:.4f} | Acc {0:.4f}'.format(100 * correct / total, ce_meter=meter.ce_meter)
            print(s)
        # if i % args.vis_freq == 0 and args.visualize:
        #     visualize(epoch, model, squeeze_layer, input_size, i, x, args, fixed_z, out_path)
        del x
        torch.cuda.empty_cache()
        gc.collect()



def validate(epoch, model, squeeze_layer, input_size, test_loader, device, args, task, ema=None):
    """
    Evaluates the cross entropy between p_data and p_model.
    """
    bpd_meter = utils.AverageMeter()
    ce_meter = utils.AverageMeter()

    if ema is not None:
        ema.swap()

    update_lipschitz(model)

    model = parallelize(model)
    model.eval()

    correct = 0
    total = 0

    start = time.time()
    with torch.no_grad():
        for i, (x, y) in enumerate(tqdm(test_loader)):
            x = x.to(device)
            logpx, logits, _, _ = compute_loss(x, model, squeeze_layer, input_size, task, args)
            bpd_normal = -torch.mean(logpx * y) / (args.imagesize * args.imagesize * args.im_dim) / np.log(2)  # pdf of
            bpd_anomaly = -torch.mean(logpx * (1-y)) / (args.imagesize * args.imagesize * args.im_dim) / np.log(
                2)  # pdf of anomalies
            bpd_meter.update(bpd_normal.item(), x.size(0))

            if task in ['classification', 'hybrid']:
                y = y.to(device)
                loss = F.cross_entropy(logits, y)
                ce_meter.update(loss.item(), x.size(0))
                _, predicted = logits.max(1)
                total += y.size(0)
                correct += predicted.eq(y).sum().item()
    val_time = time.time() - start

    if ema is not None:
        ema.swap()
    s = 'Epoch: [{0}]\tTime {1:.2f} | Test bits/dim {bpd_meter.avg:.4f}'.format(epoch, val_time, bpd_meter=bpd_meter)
    if task in ['classification', 'hybrid']:
        s += ' | CE {:.4f} | Acc {:.2f}'.format(ce_meter.avg, 100 * correct / total)
    return bpd_meter.avg


def visualize(epoch, model, squeeze_layer, input_size:int, itr, real_imgs, args, fixed_z, out_path:Optional[str]=None):
    model.eval()
    utils.makedirs(os.path.join('results/', 'imgs'))
    real_imgs = real_imgs[:32]
    _real_imgs = real_imgs

    if args.data == 'celeba_5bit':
        nvals = 32
    elif args.data == 'celebahq':
        nvals = 2**args.nbits
    else:
        nvals = 256

    with torch.no_grad():
        # reconstructed real images
        real_imgs, _ = add_padding(real_imgs, args, nvals)
        if args.squeeze_first: real_imgs = squeeze_layer(real_imgs)
        recon_imgs = model(model(real_imgs.view(-1, *input_size[1:])), inverse=True).view(-1, *input_size[1:])
        if args.squeeze_first: recon_imgs = squeeze_layer.inverse(recon_imgs)
        recon_imgs = remove_padding(recon_imgs, args)

        # random samples
        fake_imgs = model(fixed_z, inverse=True).view(-1, *input_size[1:])
        if args.squeeze_first: fake_imgs = squeeze_layer.inverse(fake_imgs)
        fake_imgs = remove_padding(fake_imgs, args)

        fake_imgs = fake_imgs.view(-1, args.im_dim, args.imagesize, args.imagesize)
        recon_imgs = recon_imgs.view(-1, args.im_dim, args.imagesize, args.imagesize)
        imgs = torch.cat([_real_imgs, fake_imgs, recon_imgs], 0)

        if out_path is None: out_path = f'{args.experiment_id}-e{epoch:03d}_i{itr:06d}.png'
        filename = os.path.join('results/', 'imgs', out_path)
        save_image(imgs.cpu().float(), filename, nrow=16, padding=2)
    model.train()

