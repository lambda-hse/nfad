import torch
import torch.nn as nn
import torch.nn.functional as F
import itertools
import numpy as np

from .. import utils

class DeepSAD_Base(nn.Module):
    def __init__(self, c: float = None, eta: float = 1.0):
        super().__init__()
        if not c is None: self.c = torch.tensor(c)
        else: self.c = None
        self.eta = eta

    def forward(self, X):
        raise NotImplemented()

    def distance(self, X):
        Z = self.forward(X)
        dist = torch.mean(torch.square(Z - self.c[None]), dim=1)
        return dist

    def loss(self, X_pos, X_neg, coef, eps=1e-6):
        """
        Deep SAD loss
        :param X_pos: a batch with positive samples;
        :param X_neg: a batch with known anomalies, None if absent.
        :return:
        """
        # labels = (0.5 - batch_y.float()) * 2 # convert labels to -1 for known anomalies, +1 for known normal samples
        dist_pos = self.distance(X_pos)
        losses_pos = torch.mean(dist_pos)

        if X_neg is None:
            return losses_pos
        else:
            dist_neg = self.distance(X_neg)
            losses_neg = torch.mean(self.eta / (dist_neg + eps))

            return losses_pos + coef * losses_neg

    def update_center_c(self, X_pos, X_neg=None, batch_size=32):
        outputs = list()

        with torch.no_grad():
            outputs.extend(
                torch.mean(self.forward(X_pos[indx]), dim=0)
                for indx in utils.seq(len(X_pos), batch_size)
            )

            if X_neg is not None:
                outputs.extend(
                    torch.mean(self.forward(X_neg[indx]), dim=0)
                    for indx in utils.seq(len(X_neg), batch_size)
                )
            outputs = torch.stack(outputs, dim=0)
            self.c = torch.mean(outputs, dim=0)

def prepare_reweighting(X_pos, X_neg, batch_size):
    if X_neg is None:
        return batch_size, None, None

    else:
        total = len(X_pos) + len(X_neg)

        if len(X_pos) > len(X_neg):
            batch_size_neg = max(
                int(len(X_neg) / total * batch_size),
                1
            )
            batch_size_pos = batch_size - batch_size_neg
        else:
            batch_size_pos = max(
                int(len(X_pos) / total * batch_size),
                1
            )
            batch_size_neg = batch_size - batch_size_pos

        coef_reweighting = len(X_neg) / len(X_pos)

        return batch_size_pos, batch_size_neg, coef_reweighting

def sample(X, batch_size):
    if X is None:
        return None
    else:
        with torch.no_grad():
            indx = torch.randint(len(X), size=(batch_size, ))
            return X[indx]

def pretrain_dsad(encoder:DeepSAD_Base, decoder:nn.Module,
                  X_pos, X_neg, batch_size=32,
                  n_epoches=100,
                  lr=1e-3,weight_decay=1e-6, progress=None):
    progress = (lambda x, **kwargs: x) if progress is None else progress

    # Pretrain encoder with autoencoder
    opt_ae = torch.optim.Adam(itertools.chain(encoder.parameters(), decoder.parameters()),
                                    lr=lr, weight_decay=weight_decay)

    batch_size_pos, batch_size_neg, coef_reweighting = prepare_reweighting(X_pos, X_neg, batch_size)
    print('Batch sizes: %d, %d, C = %.2e' % (
        batch_size_pos,
        0 if batch_size_neg is None else batch_size_neg,
        0 if batch_size_neg is None else coef_reweighting
    ))

    n_batches = len(X_pos) // batch_size
    losses = np.zeros(shape=(n_epoches, n_batches), dtype='float32')

    for i in progress(range(n_epoches), desc='pretraining'):
        for j in range(n_batches):
            X_batch_pos, X_batch_neg = sample(X_pos, batch_size_pos), sample(X_neg, batch_size_neg)

            opt_ae.zero_grad()
            X_rec_pos = decoder(encoder(X_batch_pos))
            loss_pos = F.mse_loss(X_rec_pos, X_batch_pos).mean()

            if X_batch_neg is None:
                loss = loss_pos
            else:
                X_rec_neg = decoder(encoder(X_batch_neg))
                loss_neg = F.mse_loss(X_rec_neg, X_batch_neg).mean()

                loss = loss_pos + coef_reweighting * loss_neg

            loss.backward()
            opt_ae.step()

            losses[i, j] = loss.item()

    encoder.update_center_c(X_pos, X_neg, batch_size=batch_size)

    return losses


def train_dsad(encoder: DeepSAD_Base,
               X_pos, X_neg=None,
               n_epoches=50, batch_size=32,
               lr=1e-3, weight_decay=1e-6, progress=None):
    progress = (lambda x, **kwargs: x) if progress is None else progress

    # Main Deep SAD training loop
    opt_sad = torch.optim.Adam(encoder.parameters(), lr=lr, weight_decay=weight_decay)

    batch_size_pos, batch_size_neg, coef_reweighting = prepare_reweighting(X_pos, X_neg, batch_size)

    n_batches = len(X_pos) // batch_size
    losses = np.zeros(shape=(n_epoches, n_batches), dtype='float32')

    for i in progress(range(n_epoches), desc='training'):
        for j in range(n_batches):
            X_batch_pos, X_batch_neg = sample(X_pos, batch_size_pos), sample(X_neg, batch_size_neg)

            opt_sad.zero_grad()
            loss = encoder.loss(X_batch_pos, X_batch_neg, coef_reweighting)
            loss.backward()
            opt_sad.step()

            losses[i, j] = loss.item()

    return losses

def predict_dsad(encoder: DeepSAD_Base, X_test, batch_size=32):
    with torch.no_grad():
        output = list(
            encoder.distance(X_test[indx]).cpu().numpy()
            for indx in utils.seq(len(X_test), batch_size)
        )

    return np.concatenate(output, axis=0)
