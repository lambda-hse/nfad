import os
import numpy as np

__all__ = [
  'Dataset',
  'MulticlassDataset', 'TwoClassDataset',
  'FlowMulticlassDataset', 'FlowTwoClassDataset'
]

class Dataset(object):
  def __init__(self, data_root=None):
    if data_root is None:
      self.data_root = os.environ.get('DATA_ROOT', '.')
    else:
      self.data_root = data_root

  def shape(self):
    raise NotImplementedError()

  def partition(self, partition, seed):
    raise NotImplementedError()

  def network_pool(self):
    raise NotImplementedError()

  def __str__(self):
    return self.__class__.__name__

class MulticlassDataset(Dataset):
  def __init__(self, data_pos, data_neg, labels_neg, data_test, labels_test, data_root=None):
    self.data_pos, self.data_neg = data_pos, data_neg
    self.labels_neg = labels_neg
    self.data_test, self.labels_test = data_test, labels_test

    self._max_samples_per_class = None

    super(MulticlassDataset, self).__init__(data_root)

  def shape(self):
    return (None, ) + self.data_pos.shape[1:]

  def partition(self, num_neg_classes, seed):
    from . import utils
    indx_neg = utils.partition_multiclass(
      self.labels_neg, num_neg_classes,
      max_samples_per_class=self._max_samples_per_class,
      seed=seed
    )

    if len(indx_neg) > 0:
      return self.data_pos, self.data_neg[indx_neg]
    else:
      return self.data_pos, None

  def network_pool(self):
    raise NotImplementedError()

class FlowMulticlassDataset(MulticlassDataset):
  def __init__(self, flow, pos_class, data_root=None):
    data_train, labels_train, data_test, labels_test = flow(data_root)
    labels_train = np.argmax(labels_train, axis=1)
    labels_test = np.argmax(labels_test, axis=1)

    indx_pos, = np.where(labels_train == pos_class)
    indx_neg, = np.where(labels_train != pos_class)

    data_pos = data_train[indx_pos]
    data_neg, labels_neg = data_train[indx_neg], labels_train[indx_neg]

    data_test, labels_test = data_test, np.where(labels_test == pos_class, 1, 0)

    self.pos_class = pos_class

    super(FlowMulticlassDataset, self).__init__(
      data_pos, data_neg,
      labels_neg,
      data_test, labels_test,
      data_root=data_root
    )

  def __str__(self):
    return '%s-%d' % (self.__class__.__name__, self.pos_class)

class TwoClassDataset(Dataset):
  def __init__(self, data_train, labels_train, data_test, labels_test, data_root=None):
    super(TwoClassDataset, self).__init__(data_root)
    self.data_train, self.labels_train, self.data_test, self.labels_test = \
      data_train, labels_train, data_test, labels_test

    self.indx_pos, = np.where(self.labels_train == 1)
    self.indx_neg, = np.where(self.labels_train == 0)

  def shape(self):
    return (None,) + self.data_train.shape[1:]

  def partition(self, num_samples, seed):
    from . import utils
    indx_neg = utils.partition_single_class(self.indx_neg, num_samples, seed=seed)
    if len(indx_neg) > 0:
      return self.data_train[self.indx_pos], self.data_train[indx_neg]
    else:
      return self.data_train[self.indx_pos], None

  def network_pool(self):
    raise NotImplementedError()


class FlowTwoClassDataset(TwoClassDataset):
  def __init__(self, flow, data_root=None):
    data_train, labels_train, data_test, labels_test = flow(data_root)
    super(FlowTwoClassDataset, self).__init__(data_train, labels_train, data_test, labels_test, data_root=data_root)