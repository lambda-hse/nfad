import crayflow as flow
from .meta import FlowTwoClassDataset

__all__ = [
  'SUSY'
]

susy = flow.dataflow(
  flow.instances.download_susy('SUSY/'),
  flow.instances.read_susy() @ flow.pickled('SUSY/SUSY.pickled'),
  flow.stage(name='train-test split')(
      flow.data.split(split_ratios=(8, 2), seed=111223)
  )
)

class SUSY(FlowTwoClassDataset):
  def __init__(self, data_root=None):
    super(SUSY, self).__init__(susy, data_root)

  def network_pool(self):
    from ..models import Dense_Encoder, Dense_Decoder, Dense_Classifier, Feature_Encoder, Anomaly_Detector
    from ..algorithms.nsf import build_model
    shape = self.shape()

    return dict(
      encoder=lambda: Dense_Encoder(input_dim=shape[1], latent_size=128),
      decoder=lambda: Dense_Decoder(input_dim=shape[1], latent_size=128),
      nf=build_model,
      clf=lambda: Dense_Classifier(input_dim=shape[1]),
      feature_encoder=lambda: Feature_Encoder(input_dim=shape[1]),
      anomaly_detector=lambda: Anomaly_Detector()
    )
