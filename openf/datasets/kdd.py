import numpy as np
import crayflow as flow
from .meta import FlowMulticlassDataset

__all__ = [
  'KDD'
]

def normalize(data_train, labels_train, data_test, labels_test, *args):
  for i in range(data_train.shape[1]):
    if np.unique(data_train[:, i]).shape[0] > 2:
      data_train[:, i] = np.log1p(data_train[:, i])
      data_test[:, i] = np.log1p(data_test[:, i])

  return data_train, labels_train, data_test, labels_test

kdd = flow.dataflow(
  flow.instances.download_kdd_99('KDD-99/'),
  flow.instances.read_kdd_99() @ flow.pickled('KDD-99/KDD-99.pickled'),
  flow.stage(cache=flow.pickled('KDD-99/KDD-99.normalized.pickled'), name='normalize')(
    normalize
  )
)

class KDD(FlowMulticlassDataset):
  def __init__(self, data_root=None):
    super(KDD, self).__init__(pos_class=0, flow=kdd, data_root=data_root)
    self._max_samples_per_class = 1000

  def network_pool(self):
    from ..models import Dense_Encoder, Dense_Decoder, Dense_Classifier, Feature_Encoder, Anomaly_Detector
    from ..algorithms.nsf import build_model
    shape = self.shape()

    return dict(
      encoder=lambda: Dense_Encoder(input_dim=shape[1], latent_size=128),
      decoder=lambda: Dense_Decoder(input_dim=shape[1], latent_size=128),
      nf=build_model,
      clf=lambda: Dense_Classifier(input_dim=shape[1]),
      feature_encoder=lambda: Feature_Encoder(input_dim=shape[1]),
      anomaly_detector=lambda: Anomaly_Detector()
    )
