import numpy as np

__all__ = [
  'partition_single_class',
  'partition_multiclass',
  'combine'
]

def select_class(labels, neg_class, max_samples_per_class, rng):
  indx, = np.where(labels == neg_class)

  if max_samples_per_class is None:
    return indx
  else:
    size = min([max_samples_per_class, len(indx)])
    return rng.choice(indx, size=size, replace=False)

def partition_multiclass(labels, num_neg_classes, max_samples_per_class, seed):
  if num_neg_classes == 0:
    return np.zeros(shape=(0, ), dtype=np.int64)

  rng = np.random.RandomState(seed=seed)
  all_neg_classes = np.unique(labels)

  selected_neg_classes = rng.choice(all_neg_classes, replace=False, size=num_neg_classes)

  return np.concatenate([
    select_class(labels, neg_class, max_samples_per_class, rng)
    for neg_class in selected_neg_classes
  ])

def partition_single_class(indx, num_samples, seed):
  if num_samples == 0:
    return np.zeros(shape=(0,), dtype=np.int64)

  rng = np.random.RandomState(seed=seed)
  return rng.choice(indx, size=num_samples)

def combine(data_pos, data_neg):
  if data_neg is None: data_neg = np.zeros_like(data_pos)[:0]
  X = np.concatenate([data_pos, data_neg], axis=0)
  y = np.concatenate([
    np.ones(shape=(len(data_pos), ), dtype=np.float32),
    np.zeros(shape=(len(data_neg),), dtype=np.float32)
  ])

  return X, y

