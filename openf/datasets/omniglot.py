import numpy as np
import crayflow as flow

from .utils import combine
from .meta import MulticlassDataset

__all__ = [
  'Omniglot'
]

omniglot = flow.dataflow([
  (
    flow.instances.download_omniglot('Omniglot/'),
    flow.instances.read_omniglot(return_mapping=True) @ flow.pickled('Omniglot/Omniglot.pickled'),
  ),

  (
    flow.instances.download_omniglot_test('Omniglot/'),
    flow.instances.read_omniglot(return_mapping=True) @ flow.pickled('Omniglot/Omniglot-test.pickled'),
  )
])

normal_class_mapping = {
  0: 'Braille',
  1: 'Greek',
  2: 'Futurama'
}

class Omniglot(MulticlassDataset):
  def __init__(self, pos_class, data_root=None):
    data_train, labels_train, characters_train, mapping_train, \
    data_test, labels_test, characters_test, mapping_test = \
      omniglot(data_root=data_root)

    pos_class_indx = mapping_train[normal_class_mapping[pos_class]]
    indx_pos, = np.where(labels_train == pos_class_indx)
    indx_neg, = np.where(labels_train != pos_class_indx)

    data_pos = data_train[indx_pos]
    data_neg = data_train[indx_neg]
    labels_neg = labels_train[indx_neg]

    data_pos, data_pos_test = flow.data.split(split_ratios=(1, 1), seed=123456789)(data_pos)
    data_test, labels_test = combine(data_pos_test, data_test)

    super(Omniglot, self).__init__(
      data_pos, data_neg,
      labels_neg,
      data_test, labels_test,
      data_root=data_root
    )


  def network_pool(self):
    from ..models import Omniglot_LeNet_Encoder, Omniglot_LeNet_Decoder, Anomaly_Detector
    from ..algorithms.resflow_utils import build_model

    return dict(
      encoder=lambda: Omniglot_LeNet_Encoder(),
      decoder=lambda: Omniglot_LeNet_Decoder(),
      nf=build_model,
      feature_encoder=lambda: Omniglot_LeNet_Encoder(),
      anomaly_detector=lambda: Anomaly_Detector(64)

    )
