import crayflow as flow
from .meta import FlowTwoClassDataset

__all__ = [
  'HIGGS'
]

higgs = flow.dataflow(
  flow.instances.download_higgs('HIGGS/'),
  flow.instances.read_higgs() @ flow.pickled('HIGGS/HIGGS.pickled'),
  flow.stage(name='train-test split')(
      flow.data.split(split_ratios=(10000, 1), seed=111223)
  )
)

class HIGGS(FlowTwoClassDataset):
  def __init__(self, data_root=None):
    super(HIGGS, self).__init__(higgs, data_root)

  def network_pool(self):
    from ..models import Dense_Encoder, Dense_Decoder, Dense_Classifier, Feature_Encoder, Anomaly_Detector
    from ..algorithms.nsf import build_model
    shape = self.shape()

    return dict(
      encoder=lambda: Dense_Encoder(input_dim=shape[1], latent_size=128),
      decoder=lambda: Dense_Decoder(input_dim=shape[1], latent_size=128),
      nf=build_model,
      clf=lambda: Dense_Classifier(input_dim=shape[1]),
      feature_encoder=lambda: Feature_Encoder(input_dim=shape[1]),
      anomaly_detector=lambda: Anomaly_Detector()

    )

