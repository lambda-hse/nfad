from .cifar import CIFAR
from .mnist import MNIST
from .omniglot import Omniglot

from .higgs import HIGGS
from .susy import SUSY
from .kdd import KDD

available_datasets = dict(
    mnist=MNIST,
    cifar=CIFAR,
    higgs=HIGGS,
    susy=SUSY,
    kdd=KDD,
    omniglot=Omniglot
)