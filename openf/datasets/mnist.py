import crayflow as flow
from .meta import FlowMulticlassDataset

__all__ = [
  'MNIST'
]

mnist = flow.dataflow(
  flow.instances.download_mnist('MNIST/'),
  flow.instances.read_mnist() @ flow.pickled('MNIST/MNIST.pickled')
)

class MNIST(FlowMulticlassDataset):
  def __init__(self, pos_class, data_root=None):
    super(MNIST, self).__init__(mnist, pos_class, data_root)

  def network_pool(self):
    from ..models import MNIST_LeNet_Encoder, MNIST_LeNet_Decoder, Anomaly_Detector
    from ..algorithms.resflow_utils import build_model

    return dict(
      encoder=lambda: MNIST_LeNet_Encoder(),
      decoder=lambda: MNIST_LeNet_Decoder(),
      nf=build_model,
      feature_encoder=lambda: MNIST_LeNet_Encoder(),
      anomaly_detector=lambda: Anomaly_Detector(32)
    )
