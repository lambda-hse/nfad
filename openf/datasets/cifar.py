import crayflow as flow
from .meta import FlowMulticlassDataset

__all__ = [
  'CIFAR'
]

cifar = flow.dataflow(
  flow.instances.download_cifar10('CIFAR/'),
  flow.instances.read_cifar10() @ flow.pickled('CIFAR/CIFAR-10.pickled')
)

class CIFAR(FlowMulticlassDataset):
  def __init__(self, pos_class, data_root=None):
    super(CIFAR, self).__init__(cifar, pos_class, data_root)

  def network_pool(self):
    from ..models import CIFAR10_LeNet_Encoder, CIFAR10_LeNet_Decoder, Anomaly_Detector
    from ..algorithms.resflow_utils import build_model

    return dict(
      encoder=lambda: CIFAR10_LeNet_Encoder(),
      decoder=lambda: CIFAR10_LeNet_Decoder(),
      nf=build_model,
      feature_encoder=lambda: CIFAR10_LeNet_Encoder(),
      anomaly_detector=lambda: Anomaly_Detector(128)
    )

