from .cifar10 import *
from .dense import *
from .mnist import *
from .omniglot import *