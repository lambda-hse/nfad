import torch.nn as nn
from torch import optim
from openf.algorithms.deep_sad import DeepSAD_Base
from openf.utils import init_weights
from tqdm import trange
import numpy as np
import torch

class Feature_Encoder(nn.Module):
    def __init__(self, input_dim:int):
        super().__init__()
        dim = input_dim
        self.net = nn.Sequential(
            nn.Linear(dim, 20),
            nn.ReLU(),
        )
        self.apply(init_weights)

    def forward(self, x):
        return self.net(x)

class Anomaly_Detector(nn.Module):
    def __init__(self, input_dim=20):
        super().__init__()
        self.net = nn.Sequential(
            # nn.ReLU()
            nn.Linear(2*input_dim, 1)
        )
        self.apply(init_weights)

    def forward(self, x):
        return self.net(x)


class Dense_Classifier(nn.Module):
    def __init__(self, input_dim:int):
        super().__init__()
        dim = input_dim
        self.net = nn.Sequential(
            nn.Linear(dim, 3*dim),
            nn.ReLU(),
            nn.Linear(3*dim, 2*dim),
            nn.ReLU(),
            #nn.Linear(3*dim, 2*dim),
            #nn.ReLU(),
            #nn.Linear(2*dim, 1*dim),
            #nn.ReLU(),
            nn.Linear(2*dim, 1)
        )
        self.apply(init_weights)

    def forward(self, x):
        return self.net(x)

    def fit(self, dataloader, sample_anomalies_fn, epoches=10):
        device = next(self.parameters()).device
        criterion = nn.BCEWithLogitsLoss()
        optimizer = optim.AdamW(self.parameters())
        self.train()
        losses = []
        for epoch in trange(epoches):
            for batch_X, batch_y in dataloader:
                optimizer.zero_grad()

                batch_X, batch_y = [x.to(device).float() for x in [batch_X, batch_y]]
                batch_size = batch_X.shape[0]
                # batch_y = batch_y.long()
                preds = self(batch_X).squeeze(1)
                loss = criterion(preds, batch_y)
                loss.backward()
                optimizer.step()

                optimizer.zero_grad()
                batch_y = torch.zeros(batch_size).to(device).float()
                batch_X = sample_anomalies_fn(batch_size).to(device)
                preds = self(batch_X).squeeze(1)
                loss = criterion(preds, batch_y)
                loss.backward()
            losses.append(loss.detach().cpu().item())
        return losses

    def predict(self, dataloader):
        result = []
        device = next(self.parameters()).device
        with torch.no_grad():
            for batch_X in dataloader:
                if isinstance(batch_X, list): batch_X = batch_X[0]
                preds = self(batch_X.to(device).float())
                result.append(preds.detach().cpu().numpy().squeeze())
        return np.concatenate(result)











class Dense_Encoder(DeepSAD_Base):
    def __init__(self, input_dim:int, latent_size:int):
        super(Dense_Encoder, self).__init__()
        dim = input_dim
        self.net = nn.Sequential(
            nn.Linear(dim, 4 * dim, bias=False), nn.ReLU(),
            nn.Linear(4 * dim, 3 * dim, bias=False), nn.ReLU(),
            nn.Linear(3 * dim, 2 * dim, bias=False), nn.ReLU(),
            nn.Linear(2 * dim, 1 * dim, bias=False), nn.ReLU(),
            nn.Linear(dim, latent_size, bias=False)
        )
        self.apply(init_weights)

    def forward(self, x):
        return self.net(x)

class Dense_Decoder(nn.Module):
    def __init__(self, input_dim: int, latent_size: int):
        super(Dense_Decoder, self).__init__()
        dim = input_dim
        self.net = nn.Sequential(
            nn.Linear(latent_size, dim),
            nn.ReLU(),
            nn.Linear(1 * dim, 2 * dim),
            nn.ReLU(),
            nn.Linear(2 * dim, 3 * dim),
            nn.ReLU(),
            nn.Linear(3 * dim, 4 * dim),
            nn.ReLU(),
            nn.Linear(4 * dim, dim)
        )
        self.apply(init_weights)

    def forward(self, x):
        return self.net(x)
