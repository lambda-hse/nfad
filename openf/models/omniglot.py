import torch.nn as nn
from openf.algorithms.deep_sad import DeepSAD_Base
from openf.utils import init_weights

class Omniglot_LeNet_Encoder(DeepSAD_Base):
    def __init__(self, N=16, rep_dim=64):
        super(Omniglot_LeNet_Encoder, self).__init__()

        self.net = nn.Sequential(
            nn.Conv2d(1, 2 * N, 5, stride=2, bias=False),
            nn.LeakyReLU(),

            nn.Conv2d(2 * N, 2 * N, 5, stride=2, bias=False),
            nn.BatchNorm2d(2 * N, eps=1e-04, affine=False),
            nn.LeakyReLU(),

            nn.Conv2d(2 * N, 3 * N, 2, stride=2, bias=False),
            nn.LeakyReLU(),

            nn.Conv2d(3 * N, 3 * N, 3, bias=False),
            nn.BatchNorm2d(3 * N, eps=1e-04, affine=False),
            nn.LeakyReLU(),

            nn.Conv2d(3 * N, 4 * N, 2, stride=2, bias=False),
            nn.LeakyReLU(),

            nn.Conv2d(4 * N, 4 * N, 3, bias=False),
            nn.BatchNorm2d(4 * N, eps=1e-04, affine=False),
            nn.LeakyReLU(),

            nn.Conv2d(4 * N, rep_dim, 3, bias=False),
            nn.Flatten(1)
        )

        self.net.apply(init_weights)

    def forward(self, X):
        return self.net(X)

class Omniglot_LeNet_Decoder(nn.Module):
    def __init__(self, N=16, rep_dim=64):
        super(Omniglot_LeNet_Decoder, self).__init__()

        self.net = nn.Sequential(
            nn.Unflatten(1, (rep_dim, 1, 1)),

            nn.ConvTranspose2d(rep_dim, 4 * N, 3),
            nn.LeakyReLU(),

            nn.ConvTranspose2d(4 * N, 4 * N, 3),
            nn.BatchNorm2d(4 * N, eps=1e-04, affine=False),
            nn.LeakyReLU(),

            nn.ConvTranspose2d(4 * N, 3 * N, 2, stride=2),
            nn.LeakyReLU(),

            nn.ConvTranspose2d(3 * N, 3 * N, 3),
            nn.BatchNorm2d(3 * N, eps=1e-04, affine=False),
            nn.LeakyReLU(),

            nn.ConvTranspose2d(3 * N, 2 * N, 2, stride=2),
            nn.LeakyReLU(),

            nn.ConvTranspose2d(2 * N, 2 * N, 5, stride=2),
            nn.BatchNorm2d(2 * N, eps=1e-04, affine=False),
            nn.LeakyReLU(),

            nn.ConvTranspose2d(2 * N, 1, 5, stride=2),
        )
        self.net.apply(init_weights)

    def forward(self, X):
        return self.net(X)
