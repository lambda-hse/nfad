# Official repository for "NFAD: Fixing anomaly detection using normalizing flows" paper

## Reproducibility

### Step 1 - Run the experiments

All the experiments can be reproduced by executing the scripts from root folder:

```
./scripts/<algorithm>-<dataset>.sh
```

The scripts were adapted for Slurm Workload Manager. 

For local execution use ```scripts/main.sh``` instead of ```scripts/<algorithm>-<dataset>.sh```. For instance, instead of ```scripts/pro-higgs.sh``` with content

```bash
#!/bin/bash

mkdir -p logs/

python scripts/warmup.py higgs || exit $?

for partition in 0 100 1000 10000 1000000; do
  FILENAME="PRO-HIGGS-$partition"
  sbatch --gpus=1 -c 1 --job-name="$FILENAME" --error="logs/$FILENAME.err" --output="logs/$FILENAME.out" \
    scripts/main.sh pro higgs --epoches 20 --anomalies "$partition" --device cuda "$@" &
done

wait
```

run
```
FILENAME="PRO-HIGGS-$partition" ./scripts/main.sh pro higgs --epoches 20 --anomalies "$partition" --device cuda
```

### Step 2 - Get results table

The results can be aggregated to the final table by running the following script from root folder:

```
python scripts/combine.py
```
